const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getModelMateri = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?";
        var table = ["tabel_materi","id_kelas",post.id_kelas,"id_mapel",post.id_mapel];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).json(rows);
            }
        });
    });
}

exports.getModelMateriByIdMateri = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["tabel_materi","id_materi",post.id_materi];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}