const tabel_tugas = require('../../model/tabel_tugas/get');
const tabel_statusTugas = require('../../model/tabel_tugas/tabel_status_tugas/get');

exports.getTugasByIdMapel = async function (req, res) {
        console.log(req.params);
        console.log("sini");
        let isiFinal=await [{status_tugas:[]}];
        let body = {
            id_mapel:req.params.id_mapel,
            id_siswa:req.params.id_siswa,
            status:req.params.status
        }
        let response = await tabel_tugas.getTugasByIdMapel(res,body);
        isiFinal = await response;
        for(var i=0;i<isiFinal.length;i++){
            let isiCombine = await {
                id_tugas:isiFinal[i].id_nama_tugas,
                id_siswa:body.id_siswa
            };
            let resGetStatusTugas = await tabel_statusTugas.getStatusTugasByIdTugasAndIdSiswa(res,isiCombine);
            console.log(resGetStatusTugas.length);
            if(resGetStatusTugas.length != 0){
                isiFinal[i].status_tugas= await resGetStatusTugas[0].status;
            } else {
                isiFinal[i].status_tugas= await [];
            }
        }

        res.status(200).json(isiFinal);
}

exports.getTugasByIdTugas = async function (req, res) {
    console.log(req.params);
    console.log("sini");
    let isiFinal=await [{status_tugas:[]}];
    let body = {
        id_nama_tugas:req.params.id_nama_tugas
    }
    await tabel_tugas.getTugasByIdTugas(res,body);

}


exports.getTugasByIdKelas = async function (req, res) {
    console.log(req.params);
    console.log("sini");
    let body = {
        id_kelas:req.params.id_kelas,
        status_tugas:req.params.status,
        id_mapel:req.params.id_mapel
    }
    await tabel_tugas.getTugasByIdKelas(res,body);

}

