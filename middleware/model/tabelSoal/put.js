const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putSoal = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE tabel_soal SET soal=?, jawaban=?, type_soal=?, no_urut_soal=?, file=?, point=? WHERE id_soal=?"
        var table = [post.soal,post.jawaban,post.type_soal,post.no_urut_soal,post.file,post.point,post.id_soal];
        let id_soal = post.id_soal;
        let kunci = post.kunci;
        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan"+error);
            } else {
                console.log(kunci);
                console.log(id_soal);
                var query1 = "UPDATE tabel_kunci_jawaban SET jawaban=? WHERE id_soal=?"
                var table1 = [kunci,id_soal];

                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if(error){
                        res.status(404).send("data tidak ditemukan"+error);
                    } else {
                        res.status(200).send("berhasil put data");
                    }
                });
            }
        });
    })
};