const Sequelize = require('sequelize');
const db = require('../../../../config/database/mysql');

let siswa = db.define('tabel_siswa',{
    id_siswa:Sequelize.INTEGER,
    id_kelas:Sequelize.INTEGER,
    nama_siswa:Sequelize.STRING,
    nis:Sequelize.STRING
},{
    freezeTableName:true,
    timestamps:false
});

siswa.removeAttribute('id');
module.exports=siswa;