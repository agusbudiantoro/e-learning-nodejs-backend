const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_guru = require('../../model/tabel_guru');
const tabel_akun = require('../../model/tabel_akun');

exports.registerGuru = async function (req, res) {
        var post = {
            no_urut_guru:req.body.no_urut_guru,
            nip:req.body.nip,
            nama:req.body.nama,
            tempat_tgl_lahir:req.body.tempat_tgl_lahir,
            status_wali_kelas:req.body.status_wali_kelas,
            no_hp:req.body.no_hp,
            alamat_rumah:req.body.alamat_rumah,
            pangkat_golongan:req.body.pangkat_golongan,
            tmt_cpns:req.body.tmt_cpns,
            tmt_unit_kerja:req.body.tmt_unit_kerja,
            pendidikan:req.body.pendidikan,
            jurusan:req.body.jurusan,
            status:req.body.status,
            status_guru:req.body.status_guru,
            jabatan:req.body.jabatan,
            // id_mapel:req.body.id_mapel,
            // id_kelas:req.body.id_kelas
            // foto: `${req.file.fieldname}_${Date.now()}${path.extname(req.file.originalname)}`
        }
        let password=req.body.password;
        let resDataGuru='';
        let resPromiseDataGuru='';
        try {
            resDataGuru = await tabel_guru.getDataGuru(res,post); 
            resPromiseDataGuru = await resDataGuru;  
        } catch (error) {
            res.status(404).send(error);
        
        }
        if(resPromiseDataGuru.length == 0){
            let resInsertGuru = await tabel_guru.insertDataGuru(res,post);
            let resPromiseInsertGuru = await resInsertGuru;

            let resGetAkunGuru = await tabel_akun.getDataAkun(res,post,2);
            let resPromiseGetAkunGuru = await resGetAkunGuru;
            let idPengguna = await resPromiseInsertGuru.insertId
            if(resPromiseGetAkunGuru.length == 0){
                return tabel_akun.registerAkun(res,post,2, password, idPengguna);
            } else{ 
                return tabel_akun.updateIdAkun(res,post,2,password,idPengguna)
            }
        } else {
            res.status(405).send("data sudah ada");
        }
}

exports.editGuru = async function (req, res) {
    var post = {
        id_pengguna:req.params.id_pengguna,
        no_urut_guru:req.body.no_urut_guru,
        nip:req.body.nip,
        nama:req.body.nama,
        tempat_tgl_lahir:req.body.tempat_tgl_lahir,
        status_wali_kelas:req.body.status_wali_kelas,
        no_hp:req.body.no_hp,
        alamat_rumah:req.body.alamat_rumah,
        pangkat_golongan:req.body.pangkat_golongan,
        tmt_cpns:req.body.tmt_cpns,
        tmt_unit_kerja:req.body.tmt_unit_kerja,
        pendidikan:req.body.pendidikan,
        jurusan:req.body.jurusan,
        status:req.body.status,
        status_guru:req.body.status_guru,
        jabatan:req.body.jabatan,
        // id_mapel:req.body.id_mapel,
        // id_kelas:req.body.id_kelas
        // foto: `${req.file.fieldname}_${Date.now()}${path.extname(req.file.originalname)}`
    }
    let password= await "";
    password= await req.body.password
    let resDataGuru='';
    let idAkun = await req.params.id_akun;
    try {
        resDataGuru = await tabel_guru.updateDataGuru(res,post);
        let dataAkun =await{
            id_akun:idAkun,
            password:password,
            identitas_pengguna:post.nip
        }
        await tabel_akun.updateAkun(res,dataAkun);
    } catch (error) {
        res.status(404).send(error);
    }
}

exports.changePassword = async function (req, res) {
    console.log(req.body);
    var post = {
        id_akun:req.params.id_akun,
        password:req.body.password
    }
    try {
        await tabel_akun.changePassword(res,post);
    } catch (error) {
        res.status(404).send(error);
    }
}
