const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postTambahSoal = async function(res,post,kunci){
    return new Promise(function(resolve,reject){
        let kunciJawaban = kunci;
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_soal"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                let isiKunci ={
                    id_soal:rows.insertId,
                    jawaban:kunciJawaban
                }
                var query1 = "INSERT INTO ?? SET ?";
                var table1 = ["tabel_kunci_jawaban"];
                query1 = mysql.format(query1, table1);
                connection.query(query1, isiKunci, function (error, rows) {
                    if (error) {
                        res.status(404).send(error);
                    } else {
                        res.status(200).send("berhasil menambahkan soal");
                    }
                });
            }
        });
    });
}