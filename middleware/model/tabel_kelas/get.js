const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getKelasByIdMapel = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT tabel_kelas.id_kelas,tabel_kelas.nama_kelas, tabel_kelas.urutan_kelas from tabel_kelas INNER JOIN tabel_master_mata_pelajaran ON tabel_master_mata_pelajaran.id_kelas=tabel_kelas.id_kelas WHERE tabel_master_mata_pelajaran.id_master_mapel=?"
        var table = [post.id_mapel];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getAllKelas = async function (res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM tabel_kelas"

        query = mysql.format(query);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};