const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.insertAccessToken = async function(res,data,dataAkun){

        let dataAccessToken =await {
            id_akun:dataAkun.id_akun,
            access_token:data.token,
            ip_address:data.ip_address
        }

        let dataUser = await data;

        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_access_token"];
        query = mysql.format(query, table);
        connection.query(query, dataAccessToken, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
               res.status(200).send(dataUser);     
            }
        });
}

exports.getAccessToken = async function(res,data,dataAkun){
    return new Promise(function (resolve, reject){
        console.log(dataAkun);
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_access_token", "id_akun", dataAkun.id_akun];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                console.log(rows);
                resolve(rows);
            }
        });
    });
}

exports.updateAccessToken = async function(res,data,access_token){
    return new Promise(function(resolve,reject){
            var query = "UPDATE tabel_access_token SET access_token=?,ip_address=? WHERE access_token=?";
            var table = [data.token, data.ip_address,access_token.access_token];
            let isiData = data;
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update Access token" })
                } else {
                    res.status(200).send(isiData);  
                }
            });
    });
}