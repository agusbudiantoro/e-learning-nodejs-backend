const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getDataNilaiByIdKelasIdMapelIdTugas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=?"
        var table = ["tabel_nilai", "id_kelas", post.id_kelas,"id_mapel",post.id_mapel,"id_tugas",post.id_tugas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                resolve(rows)
            }
        });
    })
};

exports.getDataNilaiByIdSiswa = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_nilai", "id_siswa", post.id_siswa];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getDataNilaiByIdSiswaIdTugas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?"
        var table = ["tabel_nilai", "id_siswa", post.id_siswa, "id_tugas", post.id_tugas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};


exports.getDataNilaiByIdKelas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_nilai", "id_kelas", post.id_kelas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getDataNilaiByIdMapel = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_nilai", "id_mapel", post.id_mapel];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getDataNilaiByIdTugas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_nilai", "id_tugas", post.id_tugas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};