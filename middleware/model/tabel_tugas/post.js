const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postModelTugas = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_nama_tugas"];
        query = mysql.format(query, table);
        console.log(post);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menambahkan tugas");
            }
        });
    });
}