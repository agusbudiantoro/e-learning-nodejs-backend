
const tabel_kunci = require('../../../model/tabelSoal/tabelKunciJawaban/get');

exports.getKunciByIdSoal = async function (req, res) {
        let body = {
            id_soal:req.params.id_soal,
        }
        // jika type soal 1 atau true maka pilihan ganda
        // jika type soal 0 atau false maka essay
        let response = await tabel_kunci.getModelKunciByIdSoal(res,body);
}
