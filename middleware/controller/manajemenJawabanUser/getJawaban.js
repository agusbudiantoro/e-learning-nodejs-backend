const tabel_get_jawabanUser = require('../../model/tabel_jawaban_user/get');

exports.getJawabanUserByIdSoalDanSiswa = async function (req, res) {
        let body = {
                id_soal:req.params.id,
                id_siswa:req.params.id_siswa
        }

        await tabel_get_jawabanUser.getJawabanByIdSoalDanSiswa(res,body);
}
