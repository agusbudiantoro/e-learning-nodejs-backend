
const get_tabel_statusTugas = require('../../../model/tabel_tugas/tabel_status_tugas/get');

exports.getStatusTugas = async function (req, res) {
    console.log(req.body);
        let body = {
            id_siswa:req.params.id_siswa,
            id_tugas:req.params.id_tugas,
        }
        let response = await get_tabel_statusTugas.getStatusTugasByIdTugasAndIdSiswa(res,body);
        await res.status(200).json(response);
}
