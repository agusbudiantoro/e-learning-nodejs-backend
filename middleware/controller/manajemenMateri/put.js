
const tabel_materi = require('../../model/tabel_materi/put');

exports.updateMateri = async function (req, res) {
        let body = {
            id_materi:req.body.id_materi,
            judul:req.body.judul,
            nama_file:(req.file == undefined)?'':req.file.filename,
            original_name:(req.file == undefined)?'':req.file.originalname
        }
        await tabel_materi.putModelMateri(res,body)
}
