
const tabel_soal = require('../../model/tabelSoal/post');

exports.tambahSoal = async function (req, res) {
        let body = {
            id_pelajaran:req.body.id_pelajaran,
            id_kelas:req.body.id_kelas,
            id_tugas:req.body.id_tugas,
            soal:req.body.soal,
            jawaban:(req.body.type_soal == 1)?req.body.jawaban:null,
            type_soal:req.body.type_soal,
            no_urut_soal:req.body.no_urut_soal,
            file:req.body.file,
            point:req.body.point
        }
        // jika type soal 1 atau true maka pilihan ganda
        // jika type soal 0 atau false maka essay
        let response = await tabel_soal.postTambahSoal(res,body,req.body.kunci);
}
