const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getFileByIdFile = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_file", "id_file", post.id_file];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                resolve(rows);
            }
        });
    })
};