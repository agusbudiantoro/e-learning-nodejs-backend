
const get_tabel_nilai = require('../../model/tabel_nilai/get');
const get_tabel_siswa = require('../../model/tabel_siswa/tabel_siswa')

exports.getNilai = async function (req, res) {
    console.log(req.body);
        let body = {
            id_mapel:req.params.id_mapel,
            id_tugas:req.params.id_tugas,
            id_kelas:req.params.id_kelas
        }
        let isi =[];
        let response = await get_tabel_nilai.getDataNilaiByIdKelasIdMapelIdTugas(res,body);
        for(var i=0; i<response.length;i++){
            let responseSiswa = await get_tabel_siswa.getDataSiswaById(res,response[i].id_siswa);
            if(responseSiswa.length != 0){
                isi.push({nilai:response[i],siswa:responseSiswa[0]})
            }
        }
        await res.status(200).json(isi);
}

exports.getNilaiByIdSiswaAndTugas = async function (req, res) {
    console.log(req.body);
        let body = {
            id_siswa:req.params.id_siswa,
            id_tugas:req.params.id_tugas
        }
        let response = await get_tabel_nilai.getDataNilaiByIdSiswaIdTugas(res,body);
        await res.status(200).json(response[0]);
}