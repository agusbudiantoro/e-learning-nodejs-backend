const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.getDataSiswa = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_siswa", "nis", post.nis];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getDataSiswaById = async function (res,id_siswa){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_siswa", "id_siswa", id_siswa];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getAllDataSiswa = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ??"
        var table = ["tabel_siswa"];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getDataAkunSiswa = async function (res,id){
    console.log(id);
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "identitas_pengguna", id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.insertDataSiswa = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_siswa"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.updateDataSiswa = async function(res,post){
    return new Promise(async function(resolve,reject){
            var query = "";
            var table=[];
                query = await "UPDATE tabel_siswa SET id_kelas=?, nama_siswa=?, nis=? WHERE id_siswa=?";
                table = await [post.id_kelas,post.nama_siswa,post.nis, post.id_pengguna];
            
            query = mysql.format(query, table);
            console.log(query);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update siswa",value:error })
                } else {
                    resolve(rows);
                }
            });
        });
}

exports.delSiswa= async function(res,id){
    return new Promise(function(resolve, reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_siswa","id_siswa",id];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                res.status(200).send({ message: "berhasil hapus data dan akun siswa" })
            }
        });
    });
}