
const tabel_materi = require('../../model/tabel_materi/delete');
const tabel_materi_get = require('../../model/tabel_materi/get');

exports.deleteMateri = async function (req, res) {
        console.log(req.file);    
        let body = {
            id_materi:req.params.id_materi
        }
        let resGetMateri = await tabel_materi_get.getModelMateriByIdMateri(res,body);
        await tabel_materi.deleteModelMateri(res,body,resGetMateri[0]);
}
