
const get_tabel_nilai = require('../../model/tabel_nilai/put');

exports.putNilaiKeseluruhan = async function (req, res) {
    console.log(req.body);
        let body = {
            nilai_keseluruhan:req.body.nilai_keseluruhan,
            id_siswa:req.body.id_siswa,
            id_tugas:req.body.id_tugas,
        }
        let response = await get_tabel_nilai.putNilaiKeseluruhan(res,body)
        await res.status(200).json(response);
}
