
const tabel_soal = require('../../model/tabelSoal/put');

exports.editSoal = async function (req, res) {
    
        let body = {
            id_soal:req.params.id_soal,
            soal:req.body.soal,
            jawaban:(req.body.type_soal == 1)?req.body.jawaban:null,
            type_soal:req.body.type_soal,
            no_urut_soal:req.body.no_urut_soal,
            kunci:req.body.kunci,
            file:req.body.file,
            point:req.body.point
        }

        await tabel_soal.putSoal(res,body);
}
