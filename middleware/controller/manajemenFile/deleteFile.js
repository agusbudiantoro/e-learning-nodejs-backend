const gettabel_file = require('../../model/tabel_file/get');
const tabel_file = require('../../model/tabel_file/delete');

exports.deleteFile = async function (req, res) {
        let body = {
            id_file:req.params.id
        }
        let resGet = await gettabel_file.getFileByIdFile(res,body);
        await tabel_file.deleteModelFile(res,body,resGet[0]);
}

