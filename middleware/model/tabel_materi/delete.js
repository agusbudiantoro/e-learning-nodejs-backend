const connection = require('../../../koneksi');
const mysql = require('mysql');
const pathHapus = './upload/file/';
const fs = require('fs')

exports.deleteModelMateri = async function(res,post,responseGet){
    return new Promise(function(resolve,reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_materi",'id_materi',post.id_materi];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                fs.unlinkSync(pathHapus+responseGet.nama_file);
                res.status(200).send("berhasil hapus materi");
            }
        });
    });
}