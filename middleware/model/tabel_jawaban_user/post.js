const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postModelJawabanSIswa = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_jawaban_siswa"];
        // console.log(post);
        // let isiPost=[];
        // for(var i=0;i<post.length;i++){
        //     isiPost.push(
        //         {
        //             id_siswa:post[i].id_siswa,
        //             id_kelas:post[i].id_kelas,
        //             id_soal:post[i].id_soal,
        //             jawaban_siswa:post[i].jawaban,
        //             type_soal:post[i].type_soal,
        //             file:
        //         }
        //     )
        // }
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}