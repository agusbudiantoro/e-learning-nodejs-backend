const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putModelMapel = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE ?? SET nama_mapel=?, id_kelas=? WHERE id_master_mapel=?"
        var table = ["tabel_master_mata_pelajaran",post.nama_mapel,post.id_kelas,post.id_master_mapel];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menambahkan mata pelajaran");
            }
        });
    });
}