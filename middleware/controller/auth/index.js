const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_guru = require('../../model/tabel_guru');
const tabel_siswa = require('../../model/tabel_siswa/tabel_siswa');
const tabel_access_token = require('../../model/tabel_access_token');
const tabel_tu = require('../../model/tabel_tu');

exports.login = async function(req,res){
    let post = {
        "identitas_pengguna":req.body.identitas_pengguna,
        "password":req.body.password
    }
 
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["tabel_akun", "identitas_pengguna", post.identitas_pengguna];

    query = mysql.format(query, table);

    connection.query(query, function (error, rows) {
        if(error){
            res.status(404).send("gagal login");
        } else {
            if(rows.length != 0){
                bcrypt.compare(post.password, rows[0].password, async function (err, result) {
                    if(result == true){
                        var token = jwt.sign({ rows }, config.secret, {
                            expiresIn: 5760
                        });
                        
                        if(rows[0].role == 1){
                            let identitas = {
                                nip:post.identitas_pengguna
                            }
                            let resGetDataTu = await tabel_tu.getDataTu(res,identitas);
                            let resFinalDataTu = await resGetDataTu;
                            let resCombine = {
                                "id_pengguna": resFinalDataTu[0].id_tu,
                                "nip": resFinalDataTu[0].nip,
                                "nama": resFinalDataTu[0].nama_tu,
                                "token": token,
                                "ip_address":ip.address(),
                                "role":rows[0].role
                            }
                            
                            let resGetAccessToken = await tabel_access_token.getAccessToken(res,resCombine,rows[0]);
                            let resPromiseAccessToken = await resGetAccessToken;
                            if(resPromiseAccessToken.length == 0){
                                await tabel_access_token.insertAccessToken(res,resCombine);
                            } else{
                                await tabel_access_token.updateAccessToken(res, resCombine,resPromiseAccessToken[0])
                            }
                        }
                        else if(rows[0].role == 2){
                            let identitas = {
                                nip:post.identitas_pengguna
                            }
                            let resGetDataGuru = await tabel_guru.getDataGuru(res,identitas);
                            let resFinalDataGuru = await resGetDataGuru;
                            let resCombine = {
                                "id_pengguna": resGetDataGuru[0].id_guru,
                                "no_urut_guru": resGetDataGuru[0].no_urut_guru,
                                "nip": resGetDataGuru[0].nip,
                                "nama": resGetDataGuru[0].nama,
                                "id_mapel":resGetDataGuru[0].id_mapel,
                                // "tempat_tgl_lahir": resGetDataGuru[0].tempat_tgl_lahir,
                                // "status_wali_kelas": resGetDataGuru[0].status_wali_kelas,
                                // "no_hp": resGetDataGuru[0].no_hp,
                                // "alamat_rumah": resGetDataGuru[0].alamat_rumah,
                                // "pangkat_golongan": resGetDataGuru[0].pangkat_golongan,
                                // "tmt_cpns": resGetDataGuru[0].tmt_cpns,
                                // "tmt_unit_kerja": resGetDataGuru[0].tmt_unit_kerja,
                                // "pendidikan": resGetDataGuru[0].pendidikan,
                                // "jurusan": resGetDataGuru[0].jurusan,
                                // "mata_pelajaran": resGetDataGuru[0].mata_pelajaran,
                                // "status": resGetDataGuru[0].status,
                                // "status_guru": resGetDataGuru[0].status_guru,
                                // "jabatan": resGetDataGuru[0].jabatan,
                                "token": token,
                                "ip_address":ip.address(),
                                "role":rows[0].role
                            }
                            
                            let resGetAccessToken = await tabel_access_token.getAccessToken(res,resCombine,rows[0]);
                            let resPromiseAccessToken = await resGetAccessToken;
                            if(resPromiseAccessToken.length == 0){
                                await tabel_access_token.insertAccessToken(res,resCombine,rows[0]);
                            } else{
                                await tabel_access_token.updateAccessToken(res, resCombine,resPromiseAccessToken[0])
                            }
                        }
                        else if(rows[0].role == 3){
                            let identitas = {
                                nis:post.identitas_pengguna
                            }
                            let resGetDataSiswa = await tabel_siswa.getDataSiswa(res,identitas);
                            let resFinalDataSiswa = await resGetDataSiswa;
                            let resCombine = {
                                "id_pengguna": resFinalDataSiswa[0].id_siswa,
                                "nis": resFinalDataSiswa[0].nis,
                                "nama": resFinalDataSiswa[0].nama_siswa,
                                "id_kelas":resFinalDataSiswa[0].id_kelas,
                                "token": token,
                                "ip_address":ip.address(),
                                "role":rows[0].role
                            }
                            
                            let resGetAccessToken = await tabel_access_token.getAccessToken(res,resCombine,rows[0]);
                            let resPromiseAccessToken = await resGetAccessToken;
                            if(resPromiseAccessToken.length == 0){
                                await tabel_access_token.insertAccessToken(res,resCombine,rows[0]);
                            } else{
                                await tabel_access_token.updateAccessToken(res, resCombine,resPromiseAccessToken[0])
                            }
                        }
                    } else {
                        res.status(404).send("gagal login, password salah");
                    }
                });
            } else {
                res.status(404).send({
                    message:"gagal login, nip atau nis salah"
                });
            }
        }
    })
}