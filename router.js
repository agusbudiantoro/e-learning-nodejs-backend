'use strict';

module.exports = function(app){
    const cors = require('cors');
    app.use(cors());
    var jsonku = require('./controller');
    app.route('/',cors())
    .get(jsonku.index);

    app.route('/tampil').get(jsonku.dataGuru);
}