const gettabel_file = require('../../model/tabel_file/get');

exports.getFile = async function (req, res) {
        let body = {
            id_file:req.params.id
        }
        let resGet = await gettabel_file.getFileByIdFile(res,body);
        if(resGet.length != 0){
            await res.status(200).json(resGet[0]);
        } else {
            await res.status(404).json("file not found");
        }
}

