const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putModelMateri = async function(res,post){
    return new Promise(function(resolve,reject){
        console.log(post);
        var query = "";
        var table = [];
        if(post.nama_file != ''){
            query = "UPDATE tabel_materi SET judul=?, nama_file=?, original_name=? WHERE id_materi=?";
            table = [post.judul,post.nama_file,post.original_name, post.id_materi];
        } else{
            query = "UPDATE tabel_materi SET judul=? WHERE id_materi=?";
            table = [post.judul,post.id_materi];
        }
        
        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menambahkan materi");
            }
        });
    });
}