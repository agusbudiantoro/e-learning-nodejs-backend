const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putModelKelas = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE tabel_kelas SET nama_kelas=?, urutan_kelas=? WHERE id_kelas=?"
        var table = [post.nama_kelas,post.urutan_kelas,post.id_kelas];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan"+error);
            } else {
                res.status(200).send("berhasil put data");
            }
        });
    });
}