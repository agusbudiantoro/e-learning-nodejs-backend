var mysql = require('mysql');

const conn = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"e-learning"
});

conn.connect((err)=>{
    if(err) throw(err);
    console.log("mysql konek");
});

module.exports = conn;

// var mysql = require('mysql');

// //buat koneksi database
// const conn = mysql.createConnection({
//     host: process.env.MYSQL_HOST,
//     user: process.env.MYSQL_USER,
//     password:process.env.MYSQL_PASSWORD,
//     database:process.env.MYSQL_DATABASE,
//     // connectionLimit: 10
// });

// conn.connect((err)=>{
//     if(err) throw err;
//     console.log('Mysql terkoneksi');
//     console.log("cek");
// });

// module.exports = conn;