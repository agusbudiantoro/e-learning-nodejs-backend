const connection = require('../../../koneksi');
const mysql = require('mysql');
const pathHapus = './upload/file/';
const fs = require('fs')

exports.deleteModelFile = async function(res,post,dataFile){
    return new Promise(function(resolve,reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_file","id_file",post.id_file];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                fs.unlinkSync(pathHapus+dataFile.nama_file);
                res.status(200).send("berhasil menghapus file");
            }
        });
    });
}