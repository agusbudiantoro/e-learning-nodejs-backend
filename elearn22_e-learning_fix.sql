-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 24, 2022 at 07:57 AM
-- Server version: 10.3.32-MariaDB
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearn22_e-learning_fix`
--

-- --------------------------------------------------------

--
-- Table structure for table `status_materi`
--

CREATE TABLE `status_materi` (
  `id_status_materi` int(20) NOT NULL,
  `id_siswa` int(20) NOT NULL,
  `id_materi` int(20) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_tugas`
--

CREATE TABLE `status_tugas` (
  `id_status_tugas` int(20) NOT NULL,
  `id_siswa` int(20) NOT NULL,
  `id_tugas` int(20) NOT NULL,
  `status` int(20) NOT NULL,
  `waktu_mulai` varchar(200) NOT NULL,
  `waktu_selesai` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_tugas`
--

INSERT INTO `status_tugas` (`id_status_tugas`, `id_siswa`, `id_tugas`, `status`, `waktu_mulai`, `waktu_selesai`) VALUES
(33, 3, 1, 1, '1638682297449', '1638682357598'),
(35, 3, 4, 0, '1638698803638', ''),
(37, 3, 5, 1, '1638811833870', '1638811893156'),
(41, 3, 7, 1, '1639283075156', '1639283096413'),
(42, 4, 8, 1, '1639886045950', '1639886105120'),
(43, 3, 10, 1, '1640393331585', '1640396956265'),
(46, 3, 11, 1, '1640428660149', '1640428749829'),
(47, 3, 12, 1, '1640499388757', '1640499485842'),
(48, 4, 15, 1, '1640846385253', '1640846918874'),
(49, 4, 16, 1, '1640850557456', '1640850698928'),
(50, 5, 28, 1, '1641817235711', '1641817288027'),
(51, 6, 28, 1, '1641817531738', '1641817557809'),
(52, 7, 28, 1, '1641817631071', '1641817664430'),
(53, 8, 28, 1, '1641818250759', '1641818289091'),
(54, 10, 27, 1, '1641818433585', '1641818466576'),
(55, 11, 27, 1, '1641818497453', '1641818516611'),
(56, 12, 27, 1, '1641818552989', '1641818571351'),
(57, 4, 29, 1, '1641818659234', '1641818678043'),
(58, 16, 26, 1, '1641818740298', '1641818774538'),
(59, 17, 26, 1, '1641818805888', '1641818835596'),
(60, 18, 26, 1, '1641818873206', '1641818896889'),
(61, 19, 26, 1, '1641819012245', '1641819030262'),
(62, 20, 23, 1, '1641819064811', '1641819085207'),
(63, 21, 23, 1, '1641819436329', '1641819449098'),
(64, 22, 23, 1, '1641819472019', '1641819495331'),
(65, 23, 23, 1, '1641819518292', '1641819531203'),
(66, 24, 25, 1, '1641819552701', '1641819588239'),
(67, 25, 25, 1, '1641819666502', '1641819680438'),
(68, 26, 25, 1, '1641819703391', '1641819719265'),
(69, 27, 25, 1, '1641819737794', '1641819750759');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_access_token`
--

CREATE TABLE `tabel_access_token` (
  `id_token` int(20) NOT NULL,
  `id_akun` int(20) NOT NULL,
  `access_token` varchar(500) NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_access_token`
--

INSERT INTO `tabel_access_token` (`id_token`, `id_akun`, `access_token`, `ip_address`) VALUES
(9, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjozLCJpZF9wZW5nZ3VuYSI6MywiaWRlbnRpdGFzX3BlbmdndW5hIjoiMTIzNDMxMjMxMjMiLCJwYXNzd29yZCI6IiQyYiQxMiRXQk1tZlpHZFNTLjNvRDliMy5NZENPV1FFSGRxNlFIdjI0ZWx1RUdUNFROV242ckhlZFhUNiIsInJvbGUiOjMsInN0YXR1cyI6MCwidHlwZSI6Ik5JUyJ9XSwiaWF0IjoxNjQwNTEzOTQ3LCJleHAiOjE2NDA1MTk3MDd9.5EOjgbAUzciKp3u9BIFZ01KRAIYk_6BFLDIWbQDNccY', '103.145.226.247'),
(10, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo0LCJpZF9wZW5nZ3VuYSI6NCwiaWRlbnRpdGFzX3BlbmdndW5hIjoiODk3MzQyMzQyNzczMiIsInBhc3N3b3JkIjoiJDJiJDEyJEFRdEZTc2lKdG9Fam1qTTEwcjFoUi4vWEd0WjREMVR5NGppblNLMWdSUUszeTRIWlJWMklxIiwicm9sZSI6MSwic3RhdHVzIjowLCJ0eXBlIjoiTklQIn1dLCJpYXQiOjE2NDIzMzg3MDQsImV4cCI6MTY0MjM0NDQ2NH0.1httbf_JD1-y5eZ9bz0wNcAXrJl50O5B0f6KAQ82uAQ', '103.145.226.247'),
(18, 5, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo1LCJpZF9wZW5nZ3VuYSI6MjIsImlkZW50aXRhc19wZW5nZ3VuYSI6IjE5NjMwMzExMTk4NjAzMjAwOCIsInBhc3N3b3JkIjoiJDJiJDEyJE9kNG1Na3d2NGYxcHpFaFlETDhjeU9FZENBalBRY3FZUDQySmJlMC5IUHlvVHJ5a3BDdktLIiwicm9sZSI6Miwic3RhdHVzIjowLCJ0eXBlIjoiTklQIn1dLCJpYXQiOjE2NDIzMzg4MzAsImV4cCI6MTY0MjM0NDU5MH0.TzvtNwjU3IHBd4bDfRms3t-Og_7BQicVOhQ--LRxG9k', '103.145.226.247'),
(19, 6, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo2LCJpZF9wZW5nZ3VuYSI6MjMsImlkZW50aXRhc19wZW5nZ3VuYSI6IjE5NjIwMzI3MTk4NDEyMjAwMSIsInBhc3N3b3JkIjoiJDJiJDEyJDE2RjVNeC9vWTVRMmtHdXMyRWdPSGV2d2xGbGhXLzRPLndxY1c5NUJFa2hqMzNaWnc2OVUuIiwicm9sZSI6Miwic3RhdHVzIjowLCJ0eXBlIjoiTklQIn1dLCJpYXQiOjE2NDA0MDc0MDksImV4cCI6MTY0MDQxMzE2OX0.MCsTP1mZEbxYrNNe1v6LdqYoGJfym4UtODPUQiwJ0V4', '172.25.176.1'),
(20, 7, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo3LCJpZF9wZW5nZ3VuYSI6NCwiaWRlbnRpdGFzX3BlbmdndW5hIjoiMTIwNjUiLCJwYXNzd29yZCI6IiQyYiQxMiROUjF6ZXprLnpRcDN1UmFMUlpuZGZPZDkydTF5aWFwaE03TFNMNHVWYkpVQlUwTkpHRXRpNiIsInJvbGUiOjMsInN0YXR1cyI6MCwidHlwZSI6Ik5JUyJ9XSwiaWF0IjoxNjQyOTgzNTI4LCJleHAiOjE2NDI5ODkyODh9.TmuGArSLZZaCVMuWWleRnzUgk5QhwGlKjyGHyvUw3aw', '103.145.226.247'),
(21, 8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo4LCJpZF9wZW5nZ3VuYSI6MjQsImlkZW50aXRhc19wZW5nZ3VuYSI6IjE5NzQxMTAxMjAxNDEyMTAwMyIsInBhc3N3b3JkIjoiJDJiJDEyJGlxLkFlTWI3bmJFdE45bWI1U3oxVU9kYjRVY3Q0TXpJWVFEVy9oYU5yUlllbGlrRnlXMEouIiwicm9sZSI6Miwic3RhdHVzIjowLCJ0eXBlIjoiTklQIn1dLCJpYXQiOjE2NDIzMzkwNjUsImV4cCI6MTY0MjM0NDgyNX0.0Ilnob4BeO41DWK8wvQTmdujoiwm4677rkWotzF8r18', '103.145.226.247'),
(22, 9, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo5LCJpZF9wZW5nZ3VuYSI6MjUsImlkZW50aXRhc19wZW5nZ3VuYSI6IjE5NjIxMTI1MTk5ODAyMTAwMSIsInBhc3N3b3JkIjoiJDJiJDEyJE1PdzlKd3lxQUJ3M3lrMUp4NTlOSC5ZYUJUUzhOQXJSbFUyalJjNE9CRzlGWDIxbzUwQW1TIiwicm9sZSI6Miwic3RhdHVzIjowLCJ0eXBlIjoiTklQIn1dLCJpYXQiOjE2NDE4MjMwMTAsImV4cCI6MTY0MTgyODc3MH0.DT_FT-Rs4UYOm8cSQ56Zxf5yqxu6NFhH-Mbqrf8MhkU', '103.145.226.247'),
(23, 18, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxOCwiaWRfcGVuZ2d1bmEiOjEzLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjA5MyIsInBhc3N3b3JkIjoiJDJiJDEyJDlUUzdwVGdXa1h3LjYxNHhjY2RNWnVxdkE4a1BXaG5CZTFHLjYzaWNqNVg5dDc5OFhMOHB1Iiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE0ODAwODMsImV4cCI6MTY0MTQ4NTg0M30.g44AyA1nOvbAKoQyGQ08DwMObez6wWeGiY6Uh2gDVAM', '103.145.226.247'),
(24, 25, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyNSwiaWRfcGVuZ2d1bmEiOjIwLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE1OCIsInBhc3N3b3JkIjoiJDJiJDEyJGZwWmNpNmVOUGZ5eVRiVVpWRFkxLmUvMU5wVVJTTE4uOHhUMGZjZHUvSEdvSDl0TnROVVVXIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTkwNTQsImV4cCI6MTY0MTgyNDgxNH0.j4gCR5BQ3JQNPKup9StUhuKnoRafLOCCnwnu5coY7nY', '103.145.226.247'),
(25, 10, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxMCwiaWRfcGVuZ2d1bmEiOjUsImlkZW50aXRhc19wZW5nZ3VuYSI6IjExOTk2IiwicGFzc3dvcmQiOiIkMmIkMTIkcGtxRnZOMXNlVFhVRXBuRDZSamNEdW11d21DRDlLTVpUME5kYVE5Q0F3S3g3bXdrUnk0VlMiLCJyb2xlIjozLCJzdGF0dXMiOjAsInR5cGUiOiJOSVMifV0sImlhdCI6MTY0MTgxNzE4OSwiZXhwIjoxNjQxODIyOTQ5fQ.NnVsfoUV9RL0ruWTQ-QkNlv8ComWoO_vPgMu53FlOfU', '103.145.226.247'),
(26, 26, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyNiwiaWRfcGVuZ2d1bmEiOjIxLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE0NyIsInBhc3N3b3JkIjoiJDJiJDEyJDFjaEMwQ0F2b0pxaWlENHJQZjNHVC5sUFFPY1ZkVkd2N2w4Q2ZQUlRnbDZucExiM21KMk1HIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTk0MjcsImV4cCI6MTY0MTgyNTE4N30.DTmRhGj8u_TQaun2UfwMeJRriMlwN7DrpyyscX7qI10', '103.145.226.247'),
(27, 17, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxNywiaWRfcGVuZ2d1bmEiOjEyLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjA1MSIsInBhc3N3b3JkIjoiJDJiJDEyJEl1bDVmZ1ZGUTg1bktibmhlbFoyRmVBeGU4dGVOek4zYnYyQjMueFd0THVMY1FuOVZCaWNPIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTg1NDQsImV4cCI6MTY0MTgyNDMwNH0.8bL8L1T3Oax_4fVoAzyx9PPiocS-NaUo1BSOT-3IRSk', '103.145.226.247'),
(28, 11, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxMSwiaWRfcGVuZ2d1bmEiOjYsImlkZW50aXRhc19wZW5nZ3VuYSI6IjEyMDAxIiwicGFzc3dvcmQiOiIkMmIkMTIkd3NQWXh3c2pGb0I2d1ZKaWZRTEs4T0d1c1NTai5nMndYRi90V0pBVHp1WHBGc3Ezc3NVNWEiLCJyb2xlIjozLCJzdGF0dXMiOjAsInR5cGUiOiJOSVMifV0sImlhdCI6MTY0MTgxNzQ4MCwiZXhwIjoxNjQxODIzMjQwfQ.iK31Xb4NFewCGoTS8X-28n9bBLtRjsiWXZ2YMJM50Ks', '103.145.226.247'),
(29, 12, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxMiwiaWRfcGVuZ2d1bmEiOjcsImlkZW50aXRhc19wZW5nZ3VuYSI6IjEyMDA1IiwicGFzc3dvcmQiOiIkMmIkMTIkcWdDLlV6eE8vWUhtLjREWGR3bXRYLko4TjFDa1c1OTNtTTlGVFpyTlIzcjg1MTJUWEl6aFMiLCJyb2xlIjozLCJzdGF0dXMiOjAsInR5cGUiOiJOSVMifV0sImlhdCI6MTY0MTgxNzYwNywiZXhwIjoxNjQxODIzMzY3fQ.tLRq-0vVCRePAxIY1EOUwVTPxGZF_dBCEBQkFS9xNiI', '103.145.226.247'),
(30, 13, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxMywiaWRfcGVuZ2d1bmEiOjgsImlkZW50aXRhc19wZW5nZ3VuYSI6IjEyMDAyIiwicGFzc3dvcmQiOiIkMmIkMTIkZmRERnZZNENOazg5R3hPVlRzWmdsT29iamtCNGtuOWtQdDcyWGlENFJSR3k3SkFWMjYxbU8iLCJyb2xlIjozLCJzdGF0dXMiOjAsInR5cGUiOiJOSVMifV0sImlhdCI6MTY0MTgxODIzOSwiZXhwIjoxNjQxODIzOTk5fQ.ezzproBCGo1AMEy4pkTubpVzFft_WdT52LHcPRnrFPc', '103.145.226.247'),
(31, 15, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxNSwiaWRfcGVuZ2d1bmEiOjEwLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjA1MCIsInBhc3N3b3JkIjoiJDJiJDEyJHdXeDlSL1Vad281cUFTYmJuM0VPY094MFpGYkNRM0pFM21MYW82bGpQNXVYdFduWEdkUVhLIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTg0MjIsImV4cCI6MTY0MTgyNDE4Mn0.Gau6lSonKdwuN2mBG5tHDIedSAdnehVInedCxpohWUA', '103.145.226.247'),
(32, 16, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxNiwiaWRfcGVuZ2d1bmEiOjExLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjAzOSIsInBhc3N3b3JkIjoiJDJiJDEyJEZnRVp3dXZzMi9GeHIvREpNQ0ozdi50S1dDclBZUFpIb1cucS4uVzNaTU1UV2c3R3YwOFY2Iiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTg0ODgsImV4cCI6MTY0MTgyNDI0OH0.m7ik1r1Yru9gpYpDVeUPH6Ge5GY6txua4wiL_k1P-QE', '103.145.226.247'),
(33, 21, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyMSwiaWRfcGVuZ2d1bmEiOjE2LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjExMiIsInBhc3N3b3JkIjoiJDJiJDEyJDFCcjBHMVV3Nk9iTW8vNkJqWkMzTC5yd0liUUduTWMySjRhN2lMYlZnU04yZk84RmpCdmZPIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MjQxOTUsImV4cCI6MTY0MTgyOTk1NX0.PbvjKw44iqVnTWr2oewRonpvQEkW7DArwJohpwOTq0Y', '103.145.226.247'),
(34, 22, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyMiwiaWRfcGVuZ2d1bmEiOjE3LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjEzMCIsInBhc3N3b3JkIjoiJDJiJDEyJC81WjZiSFZxbWFTVTljMHpxdUdFa09NYktrTHJvZ3ExT3pMdHdNZDVwNDNJTTFZaDdsZnZXIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MjQ0MDksImV4cCI6MTY0MTgzMDE2OX0.FIqsfsPnQkf565f399YIOLFu5SMVh1Pr4LBHGJcMdmk', '103.145.226.247'),
(35, 23, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyMywiaWRfcGVuZ2d1bmEiOjE4LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjEyNiIsInBhc3N3b3JkIjoiJDJiJDEyJFU1RXhkUDFMLjlBUTgvUUdVV0xOak9NeDZmeDVDWVIvY25nR1UueC9ESlhYM3MvL2FDRGR5Iiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTg4NjIsImV4cCI6MTY0MTgyNDYyMn0.5D7hWVjwKjAHL0qqt3AW-CAiC5z5ChdDGILIWfRnLA8', '103.145.226.247'),
(36, 24, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyNCwiaWRfcGVuZ2d1bmEiOjE5LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjExMSIsInBhc3N3b3JkIjoiJDJiJDEyJDlTVHJUVjIwYkRoT2pONzJ3Q2hCdXUyTXRqbk9FMmFwVW5ScFNtM2cyVm1VL2xZSWtBTkphIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTkwMDMsImV4cCI6MTY0MTgyNDc2M30.J0BA4CcMM55oIEw53g3o5sgNK8JpdlbOjQ_WXM82ses', '103.145.226.247'),
(37, 27, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyNywiaWRfcGVuZ2d1bmEiOjIyLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE0OSIsInBhc3N3b3JkIjoiJDJiJDEyJEQvUkFwVjduT0VML0VELlBzL3JvZ2VUMzNFQWZJd1l2VkNQSlFXZVlQaXgxZkVsVGllYVJPIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTk0NjMsImV4cCI6MTY0MTgyNTIyM30.XBxY4qrIxAm0P0ESATpko4SkBbp-pIPlKEujkZ3P78E', '103.145.226.247'),
(38, 28, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyOCwiaWRfcGVuZ2d1bmEiOjIzLCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjEzNyIsInBhc3N3b3JkIjoiJDJiJDEyJEdvQWpNdXIzODJsU3pCU0RBOTlqbXVPUmovbjl4STZwellLcmtJQkpVLmtTaHdMOFU3MGppIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTk1MDgsImV4cCI6MTY0MTgyNTI2OH0.8MOyH29PYT7VqEVgoYlpqHGjfNkpnqGxbP9-QqCfg_I', '103.145.226.247'),
(39, 29, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoyOSwiaWRfcGVuZ2d1bmEiOjI0LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE5MiIsInBhc3N3b3JkIjoiJDJiJDEyJHdOWWxmVHgxbldzZ2dOUHREaGt3WS5JQnRhSE5CV09EZFhkV243YXZ1S2ViMDBtVUF1cWNpIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MjQ0NTksImV4cCI6MTY0MTgzMDIxOX0.UCQlMponrehEO6lZTpkjuziyAgbYL_myqUo93m7KneI', '103.145.226.247'),
(40, 30, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjozMCwiaWRfcGVuZ2d1bmEiOjI1LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE4MCIsInBhc3N3b3JkIjoiJDJiJDEyJHgvVkc4akYvd01Id0FjMmMzR1F4ei56UlBySDl4UDExczk3Z3N4NkJWNXNPSkhwOHVuRHQ2Iiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTk2NTUsImV4cCI6MTY0MTgyNTQxNX0.BF18FHoRAwzrwYQ9tWF_l3y4c0GXoAkk9ryRuSmkRdw', '103.145.226.247'),
(41, 31, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjozMSwiaWRfcGVuZ2d1bmEiOjI2LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE5MyIsInBhc3N3b3JkIjoiJDJiJDEyJC5QNFVXOUVmMUZoL0V0NjBVLjZCUi5MWjlVTjlWN1EyMFcvcU41Wko0Um5GbDVUbkw3MXRLIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MjQ1MjAsImV4cCI6MTY0MTgzMDI4MH0.pOV_jAHH4ZNcRUIKN7Qazd-dqobMO2rqBdMJLzDGdgo', '103.145.226.247'),
(42, 32, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjozMiwiaWRfcGVuZ2d1bmEiOjI3LCJpZGVudGl0YXNfcGVuZ2d1bmEiOiIxMjE4NyIsInBhc3N3b3JkIjoiJDJiJDEyJEU2TEZQTm1qQnViQlhVa3g3cGR6TC5ERDNkbFRHOGx3OFBWbVBMU2hhRmRiWjk5cjhHbkYuIiwicm9sZSI6Mywic3RhdHVzIjowLCJ0eXBlIjoiTklTIn1dLCJpYXQiOjE2NDE4MTk3MjksImV4cCI6MTY0MTgyNTQ4OX0.GEzqkJpZlQnQgTLzmmmzuGlPlwxuQYKe1ImIHlT0yu8', '103.145.226.247');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_akun`
--

CREATE TABLE `tabel_akun` (
  `id_akun` int(20) NOT NULL,
  `id_pengguna` int(20) NOT NULL,
  `identitas_pengguna` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` int(2) NOT NULL,
  `status` int(2) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_akun`
--

INSERT INTO `tabel_akun` (`id_akun`, `id_pengguna`, `identitas_pengguna`, `password`, `role`, `status`, `type`) VALUES
(4, 4, '8973423427732', '$2b$12$AQtFSsiJtoEjmjM10r1hR./XGtZ4D1Ty4jinSK1gRQK3y4HZRV2Iq', 1, 0, 'NIP'),
(5, 22, '196303111986032008', '$2b$12$Od4mMkwv4f1pzEhYDL8cyOEdCAjPQcqYP42Jbe0.HPyoTrykpCvKK', 2, 0, 'NIP'),
(7, 4, '12065', '$2b$12$NR1zezk.zQp3uRaLRZndfOd92u1yiaphM7LSL4uVbJUBU0NJGEti6', 3, 0, 'NIS'),
(8, 24, '197411012014121003', '$2b$12$iq.AeMb7nbEtN9mb5Sz1UOdb4Uct4MzIYQDW/haNrRYelikFyW0J.', 2, 0, 'NIP'),
(9, 25, '196211251998021001', '$2b$12$MOw9JwyqABw3yk1Jx59NH.YaBTS8NArRlU2jRc4OBG9FX21o50AmS', 2, 0, 'NIP'),
(10, 5, '11996', '$2b$12$pkqFvN1seTXUEpnD6RjcDumuwmCD9KMZT0NdaQ9CAwKx7mwkRy4VS', 3, 0, 'NIS'),
(11, 6, '12001', '$2b$12$wsPYxwsjFoB6wVJifQLK8OGusSSj.g2wXF/tWJATzuXpFsq3ssU5a', 3, 0, 'NIS'),
(12, 7, '12005', '$2b$12$qgC.UzxO/YHm.4DXdwmtX.J8N1CkW593mM9FTZrNR3r8512TXIzhS', 3, 0, 'NIS'),
(13, 8, '12002', '$2b$12$fdDFvY4CNk89GxOVTsZglOobjkB4kn9kPt72XiD4RRGy7JAV261mO', 3, 0, 'NIS'),
(14, 9, ' 12046', '$2b$12$vzqtRMe4M.aJ/P0CLT/VT.dJkOFJKRYkGl2td0NfQRCGZb52ctjAG', 3, 0, 'NIS'),
(15, 10, '12050', '$2b$12$wWx9R/UZwo5qASbbn3EOcOx0ZFbCQ3JE3mLao6ljP5uXtWnXGdQXK', 3, 0, 'NIS'),
(16, 11, '12039', '$2b$12$FgEZwuvs2/Fxr/DJMCJ3v.tKWCrPYPZHoW.q..W3ZMMTWg7Gv08V6', 3, 0, 'NIS'),
(17, 12, '12051', '$2b$12$Iul5fgVFQ85nKbnhelZ2FeAxe8teNzN3bv2B3.xWtLuLcQn9VBicO', 3, 0, 'NIS'),
(18, 13, '12093', '$2b$12$9TS7pTgWkXw.614xccdMZuqvA8kPWhnBe1G.63icj5X9t798XL8pu', 3, 0, 'NIS'),
(19, 14, '12086', '$2b$12$ru15xhAXZriza6ut0.7wDuZMy5Dd.vM7khszmWoNe5PCDp/IYshOm', 3, 0, 'NIS'),
(20, 15, ' 12078', '$2b$12$0Pe3O6UDwaaqZot.AU0A8OMsy7my11G9veczsAn2b9o8wy50gT1XO', 3, 0, 'NIS'),
(21, 16, '12112', '$2b$12$1Br0G1Uw6ObMo/6BjZC3L.rwIbQGnMc2J4a7iLbVgSN2fO8FjBvfO', 3, 0, 'NIS'),
(22, 17, '12130', '$2b$12$/5Z6bHVqmaSU9c0zquGEkOMbKkLrogq1OzLtwMd5p43IM1Yh7lfvW', 3, 0, 'NIS'),
(23, 18, '12126', '$2b$12$U5ExdP1L.9AQ8/QGUWLNjOMx6fx5CYR/cngGU.x/DJXX3s//aCDdy', 3, 0, 'NIS'),
(24, 19, '12111', '$2b$12$9STrTV20bDhOjN72wChBuu2MtjnOE2apUnRpSm3g2VmU/lYIkANJa', 3, 0, 'NIS'),
(25, 20, '12158', '$2b$12$fpZci6eNPfyyTbUZVDY1.e/1NpURSLN.8xT0fcdu/HGoH9tNtNUUW', 3, 0, 'NIS'),
(26, 21, '12147', '$2b$12$1chC0CAvoJqiiD4rPf3GT.lPQOcVdVGv7l8CfPRTgl6npLb3mJ2MG', 3, 0, 'NIS'),
(27, 22, '12149', '$2b$12$D/RApV7nOEL/ED.Ps/rogeT33EAfIwYvVCPJQWeYPix1fElTieaRO', 3, 0, 'NIS'),
(28, 23, '12137', '$2b$12$GoAjMur382lSzBSDA99jmuORj/n9xI6pzYKrkIBJU.kShwL8U70ji', 3, 0, 'NIS'),
(29, 24, '12192', '$2b$12$wNYlfTx1nWsggNPtDhkwY.IBtaHNBWODdXdWn7avuKeb00mUAuqci', 3, 0, 'NIS'),
(30, 25, '12180', '$2b$12$x/VG8jF/wMHwAc2c3GQxz.zRPrH9xP11s97gsx6BV5sOJHp8unDt6', 3, 0, 'NIS'),
(31, 26, '12193', '$2b$12$.P4UW9Ef1Fh/Et60U.6BR.LZ9UN9V7Q20W/qN5ZJ4RnFl5TnL71tK', 3, 0, 'NIS'),
(32, 27, '12187', '$2b$12$E6LFPNmjBubBXUkx7pdzL.DD3dlTG8lw8PVmPLShaFdbZ99r8GnF.', 3, 0, 'NIS');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_file`
--

CREATE TABLE `tabel_file` (
  `id_file` int(20) NOT NULL,
  `nama_file` varchar(500) NOT NULL,
  `original_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_file`
--

INSERT INTO `tabel_file` (`id_file`, `nama_file`, `original_name`) VALUES
(12, 'file_1638682313599.xlsx', 'data.xlsx'),
(13, 'file_1638712037347.xlsx', 'data.xlsx'),
(15, 'file_1639282810584.xlsx', 'data.xlsx'),
(16, 'file_1639283088512.xlsx', 'data.xlsx'),
(17, 'file_1639886071509.xlsx', 'data.xlsx'),
(18, 'file_1640391829079.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(19, 'file_1640391994953.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(20, 'file_1640392095814.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(22, 'file_1640393087992.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(25, 'file_1640424248703.pdf', 'Imconnect Enterprise.pdf'),
(27, 'file_1640424471052.pdf', 'Imconnect Enterprise.pdf'),
(28, 'file_1640428727552.pdf', 'Imconnect Enterprise.pdf'),
(29, 'file_1640499313663.pdf', 'METODE METODE PEKERJAAN SOSIAL.pdf'),
(30, 'file_1640499454483.pdf', 'INDIVIDU+DAN+MASYARAKAT.pdf'),
(31, 'file_1640499481049.pdf', 'INDIVIDU+DAN+MASYARAKAT.pdf'),
(32, 'file_1640843034101.pdf', 'SOAl TUGAS  SENI BUDAYA.pdf'),
(33, 'file_1641517748650.pdf', 'invoice.pdf'),
(34, 'file_1641517961233.pdf', 'invoice.pdf'),
(35, 'file_1641518081170.pdf', 'invoice.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_guru`
--

CREATE TABLE `tabel_guru` (
  `id_guru` int(11) NOT NULL,
  `no_urut_guru` int(4) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `tempat_tgl_lahir` varchar(50) NOT NULL,
  `status_wali_kelas` tinyint(1) NOT NULL,
  `no_hp` int(11) NOT NULL,
  `alamat_rumah` varchar(500) NOT NULL,
  `pangkat_golongan` varchar(80) NOT NULL,
  `tmt_cpns` date NOT NULL,
  `tmt_unit_kerja` date NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_guru` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_mapel` int(20) NOT NULL,
  `id_kelas` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_guru`
--

INSERT INTO `tabel_guru` (`id_guru`, `no_urut_guru`, `nip`, `nama`, `tempat_tgl_lahir`, `status_wali_kelas`, `no_hp`, `alamat_rumah`, `pangkat_golongan`, `tmt_cpns`, `tmt_unit_kerja`, `pendidikan`, `jurusan`, `status`, `status_guru`, `jabatan`, `id_mapel`, `id_kelas`) VALUES
(22, 4, '196303111986032008', 'NANCY TARALINA RITONGA, S.Pd', 'MEDAN;11-03-1963', 0, 0, 'PAMULANG PERMAI 2 JLN BENDA BARAT 14 BLOK B 13/3 RT 009 /RW 010 TANGSEL BANTEN', 'Pembina Tingkat 1/IV.b', '1967-12-27', '2015-12-27', 'S1', 'Pendidikan', 'PNS', 'INDUK', 'Guru SBK', 7, 8),
(24, 2, '197411012014121003', 'H. LELALAHI, S.Pd', 'JAKARTA;1974-11-01', 0, 2147483647, 'JL. DELMAN UTAMA NO. 94 RT. 012 RW. 009,  KEBAYORAN LAMA UTARA, JAKARTA SELATAN', 'Penata Muda/III.a', '2014-01-07', '2001-05-02', 'S1/PENDIDIKAN', 'Matematika', 'PNS', 'INDUK', 'Guru', 6, 7),
(25, 4, '196211251998021001', 'Drs. SUMIYANTA', 'KLATEN, 25 NOVERMBER 1962;null', 0, 2147483647, 'KP. KEBUN KOPI NO. 121 RT. 001 RW. 004 , PD. BETUNG - PD. AREN, KOTA TANGERANG SELATAN', 'Pembina/IV.a', '1998-01-28', '1996-12-27', 'S1/PENDIDIKAN', 'Sejarah', 'PNS', 'INDUK', 'Wakil Kepala Sekolah Bidang Kesiswaan', 15, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_jawaban_siswa`
--

CREATE TABLE `tabel_jawaban_siswa` (
  `id_jawaban_siswa` int(10) NOT NULL,
  `id_siswa` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_soal` int(10) NOT NULL,
  `jawaban` text NOT NULL,
  `point` int(10) NOT NULL,
  `type_soal` tinyint(1) NOT NULL,
  `id_tugas` int(20) NOT NULL,
  `id_pelajaran` int(20) NOT NULL,
  `file` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_jawaban_siswa`
--

INSERT INTO `tabel_jawaban_siswa` (`id_jawaban_siswa`, `id_siswa`, `id_kelas`, `id_soal`, `jawaban`, `point`, `type_soal`, `id_tugas`, `id_pelajaran`, `file`) VALUES
(39, 3, 5, 15, 'A', 0, 1, 7, 1, NULL),
(40, 3, 5, 16, 'C', 0, 1, 7, 1, NULL),
(41, 3, 5, 17, '3', 0, 0, 7, 1, 16),
(42, 4, 6, 18, 'A', 0, 1, 8, 5, NULL),
(43, 4, 6, 19, 'C', 0, 1, 8, 5, NULL),
(44, 4, 6, 20, 'coba 1', 0, 0, 8, 5, 17),
(45, 3, 5, 21, 'B', 0, 1, 10, 1, NULL),
(46, 3, 5, 22, 'B', 0, 1, 10, 1, NULL),
(47, 3, 5, 23, 'D', 0, 1, 10, 1, NULL),
(65, 3, 5, 24, 'A', 20, 1, 11, 1, NULL),
(66, 3, 5, 25, 'dua', 0, 0, 11, 1, 28),
(67, 3, 5, 26, 'tiga', 0, 0, 11, 1, 28),
(68, 3, 5, 27, 'C', 10, 1, 11, 1, NULL),
(69, 3, 5, 28, 'A', 20, 1, 11, 1, NULL),
(70, 3, 5, 29, '', 0, 0, 12, 1, 31),
(71, 4, 8, 30, 'B', 1, 1, 15, 7, NULL),
(72, 4, 8, 31, '1.	Seni budaya adalah segala hal yang diciptakan oleh manusia berkaitan dengan cara hidup dan berkembang secara bersama-sama pada suatu bentuk kelompok sosial yang mempunyai unsur keindahan (estetika) secara turun temurun dari generasi ke generasi.', 0, 0, 15, 7, 0),
(73, 4, 8, 34, 'C', 10, 1, 16, 7, NULL),
(74, 4, 8, 33, 'praktis dan isntan', 0, 0, 16, 7, 0),
(75, 5, 7, 88, 'A', 20, 1, 28, 8, NULL),
(76, 5, 7, 89, 'B', 20, 1, 28, 8, NULL),
(77, 5, 7, 90, 'D', 20, 1, 28, 8, NULL),
(78, 5, 7, 91, 'D', 20, 1, 28, 8, NULL),
(79, 5, 7, 92, 'webcam', 0, 0, 28, 8, 0),
(80, 6, 7, 88, 'A', 20, 1, 28, 8, NULL),
(81, 6, 7, 89, 'B', 20, 1, 28, 8, NULL),
(82, 6, 7, 90, 'D', 20, 1, 28, 8, NULL),
(83, 6, 7, 91, 'D', 20, 1, 28, 8, NULL),
(84, 6, 7, 92, 'video call', 0, 0, 28, 8, 0),
(85, 7, 7, 88, 'B', 20, 1, 28, 8, NULL),
(86, 7, 7, 89, 'B', 20, 1, 28, 8, NULL),
(87, 7, 7, 90, 'D', 20, 1, 28, 8, NULL),
(88, 7, 7, 91, 'B', 20, 1, 28, 8, NULL),
(89, 7, 7, 92, 'video call', 0, 0, 28, 8, 0),
(90, 8, 7, 88, 'B', 20, 1, 28, 8, NULL),
(91, 8, 7, 89, 'B', 20, 1, 28, 8, NULL),
(92, 8, 7, 90, 'D', 20, 1, 28, 8, NULL),
(93, 8, 7, 91, 'C', 20, 1, 28, 8, NULL),
(94, 8, 7, 92, 'video call', 0, 0, 28, 8, 0),
(95, 10, 8, 84, 'B', 20, 1, 27, 10, NULL),
(96, 10, 8, 85, '', 20, 1, 27, 10, NULL),
(97, 10, 8, 87, 'Video call', 0, 0, 27, 10, 0),
(98, 11, 8, 84, 'B', 20, 1, 27, 10, NULL),
(99, 11, 8, 87, 'webcam', 0, 0, 27, 10, 0),
(100, 12, 8, 83, '', 20, 1, 27, 10, NULL),
(101, 12, 8, 84, 'B', 20, 1, 27, 10, NULL),
(102, 12, 8, 85, '', 20, 1, 27, 10, NULL),
(103, 12, 8, 87, 'webcam', 0, 0, 27, 10, 0),
(104, 4, 9, 93, 'B', 20, 1, 29, 11, NULL),
(105, 4, 9, 94, 'B', 20, 1, 29, 11, NULL),
(106, 4, 9, 95, 'D', 20, 1, 29, 11, NULL),
(107, 4, 9, 96, 'D', 20, 1, 29, 11, NULL),
(108, 4, 9, 97, 'webcam', 0, 0, 29, 11, 0),
(109, 16, 10, 73, 'A', 20, 1, 26, 17, NULL),
(110, 16, 10, 74, 'D', 20, 1, 26, 17, NULL),
(111, 16, 10, 75, 'D', 20, 1, 26, 17, NULL),
(112, 16, 10, 76, '', 20, 1, 26, 17, NULL),
(113, 16, 10, 77, 'kedudukan', 0, 0, 26, 17, 0),
(114, 17, 10, 73, 'A', 20, 1, 26, 17, NULL),
(115, 17, 10, 74, 'D', 20, 1, 26, 17, NULL),
(116, 17, 10, 75, 'C', 20, 1, 26, 17, NULL),
(117, 17, 10, 76, '', 20, 1, 26, 17, NULL),
(118, 17, 10, 77, 'kedudukan', 0, 0, 26, 17, 0),
(119, 18, 10, 73, 'A', 20, 1, 26, 17, NULL),
(120, 18, 10, 74, 'B', 20, 1, 26, 17, NULL),
(121, 18, 10, 75, 'C', 20, 1, 26, 17, NULL),
(122, 18, 10, 76, '', 20, 1, 26, 17, NULL),
(123, 18, 10, 77, 'kedudukan', 0, 0, 26, 17, 0),
(124, 19, 10, 73, 'A', 20, 1, 26, 17, NULL),
(125, 19, 10, 74, 'D', 20, 1, 26, 17, NULL),
(126, 19, 10, 75, 'D', 20, 1, 26, 17, NULL),
(127, 19, 10, 76, '', 20, 1, 26, 17, NULL),
(128, 19, 10, 77, 'kedudukan', 0, 0, 26, 17, 0),
(129, 20, 11, 78, '', 20, 1, 23, 18, NULL),
(130, 20, 11, 79, 'D', 20, 1, 23, 18, NULL),
(131, 20, 11, 80, '', 20, 1, 23, 18, NULL),
(132, 20, 11, 81, '', 20, 1, 23, 18, NULL),
(133, 20, 11, 82, 'kedudukan', 0, 0, 23, 18, 0),
(134, 21, 11, 78, 'A', 20, 1, 23, 18, NULL),
(135, 21, 11, 79, 'D', 20, 1, 23, 18, NULL),
(136, 21, 11, 80, 'D', 20, 1, 23, 18, NULL),
(137, 21, 11, 81, 'D', 20, 1, 23, 18, NULL),
(138, 21, 11, 82, 'kedudukan', 0, 0, 23, 18, 0),
(139, 22, 11, 78, 'A', 20, 1, 23, 18, NULL),
(140, 22, 11, 79, 'D', 20, 1, 23, 18, NULL),
(141, 22, 11, 80, 'C', 20, 1, 23, 18, NULL),
(142, 22, 11, 81, 'B', 20, 1, 23, 18, NULL),
(143, 22, 11, 82, 'kedudukan', 0, 0, 23, 18, 0),
(144, 23, 11, 78, 'A', 20, 1, 23, 18, NULL),
(145, 23, 11, 79, 'D', 20, 1, 23, 18, NULL),
(146, 23, 11, 80, 'C', 20, 1, 23, 18, NULL),
(147, 23, 11, 81, 'D', 20, 1, 23, 18, NULL),
(148, 23, 11, 82, 'kedudukan', 0, 0, 23, 18, 0),
(149, 24, 12, 68, 'A', 20, 1, 25, 19, NULL),
(150, 24, 12, 69, 'D', 20, 1, 25, 19, NULL),
(151, 24, 12, 70, 'D', 20, 1, 25, 19, NULL),
(152, 24, 12, 71, 'D', 20, 1, 25, 19, NULL),
(153, 24, 12, 72, 'umur', 0, 0, 25, 19, 0),
(154, 25, 12, 68, 'A', 20, 1, 25, 19, NULL),
(155, 25, 12, 69, 'B', 20, 1, 25, 19, NULL),
(156, 25, 12, 70, 'C', 20, 1, 25, 19, NULL),
(157, 25, 12, 71, 'D', 20, 1, 25, 19, NULL),
(158, 25, 12, 72, 'umur', 0, 0, 25, 19, 0),
(159, 26, 12, 68, 'A', 20, 1, 25, 19, NULL),
(160, 26, 12, 69, 'D', 20, 1, 25, 19, NULL),
(161, 26, 12, 70, 'D', 20, 1, 25, 19, NULL),
(162, 26, 12, 71, 'D', 20, 1, 25, 19, NULL),
(163, 26, 12, 72, 'usia', 0, 0, 25, 19, 0),
(164, 27, 12, 68, 'A', 20, 1, 25, 19, NULL),
(165, 27, 12, 69, 'D', 20, 1, 25, 19, NULL),
(166, 27, 12, 70, 'D', 20, 1, 25, 19, NULL),
(167, 27, 12, 71, 'D', 20, 1, 25, 19, NULL),
(168, 27, 12, 72, 'kedudukan', 0, 0, 25, 19, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kelas`
--

CREATE TABLE `tabel_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(50) NOT NULL,
  `urutan_kelas` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_kelas`
--

INSERT INTO `tabel_kelas` (`id_kelas`, `nama_kelas`, `urutan_kelas`) VALUES
(7, '7A', 1),
(8, '7B', 2),
(9, '7C', 3),
(10, '7D', 4),
(11, '7E', 5),
(12, '7F', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kunci_jawaban`
--

CREATE TABLE `tabel_kunci_jawaban` (
  `id_kunci_jawaban` int(10) NOT NULL,
  `id_soal` int(10) NOT NULL,
  `jawaban` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_kunci_jawaban`
--

INSERT INTO `tabel_kunci_jawaban` (`id_kunci_jawaban`, `id_soal`, `jawaban`) VALUES
(5, 15, 'A'),
(6, 16, 'C'),
(7, 17, ''),
(8, 18, 'B'),
(9, 19, 'C'),
(10, 20, ''),
(11, 21, 'B'),
(12, 22, 'B'),
(13, 23, 'D'),
(14, 24, 'A'),
(15, 25, ''),
(16, 26, ''),
(17, 27, 'C'),
(18, 28, 'B'),
(19, 29, ''),
(20, 30, 'B'),
(23, 33, 'C'),
(24, 34, 'C'),
(25, 35, 'A'),
(26, 36, 'B'),
(27, 37, 'D'),
(28, 38, 'D'),
(29, 39, ''),
(30, 40, 'A'),
(31, 41, 'B'),
(32, 42, 'D'),
(33, 43, 'D'),
(34, 44, ''),
(35, 45, 'A'),
(36, 46, 'B'),
(37, 47, 'D'),
(38, 48, 'D'),
(39, 49, ''),
(40, 50, 'A'),
(41, 51, 'D'),
(42, 52, 'D'),
(43, 53, 'D'),
(44, 54, ''),
(45, 55, 'A'),
(46, 56, 'D'),
(47, 57, 'D'),
(48, 58, 'D'),
(49, 59, ''),
(50, 60, 'A'),
(51, 61, 'D'),
(52, 62, 'D'),
(53, 63, 'D'),
(54, 64, ''),
(57, 67, 'A'),
(58, 68, 'A'),
(59, 69, 'D'),
(60, 70, 'D'),
(61, 71, 'D'),
(62, 72, ''),
(63, 73, 'A'),
(64, 74, 'D'),
(65, 75, 'D'),
(66, 76, 'D'),
(67, 77, ''),
(68, 78, 'A'),
(69, 79, 'D'),
(70, 80, 'D'),
(71, 81, 'D'),
(72, 82, ''),
(73, 83, 'A'),
(74, 84, 'B'),
(75, 85, 'D'),
(76, 86, 'D'),
(77, 87, ''),
(78, 88, 'A'),
(79, 89, 'B'),
(80, 90, 'D'),
(81, 91, 'D'),
(82, 92, ''),
(83, 93, 'A'),
(84, 94, 'B'),
(85, 95, 'D'),
(86, 96, 'D'),
(87, 97, '');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_list_kelas_guru`
--

CREATE TABLE `tabel_list_kelas_guru` (
  `id_list_kelas` int(20) NOT NULL,
  `id_guru` int(20) NOT NULL,
  `id_kelas` int(20) NOT NULL,
  `id_mapel` int(20) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_list_kelas_guru`
--

INSERT INTO `tabel_list_kelas_guru` (`id_list_kelas`, `id_guru`, `id_kelas`, `id_mapel`, `status`) VALUES
(11, 22, 7, 6, 0),
(12, 22, 8, 7, 0),
(13, 22, 0, 0, 0),
(15, 22, 8, 7, 0),
(17, 24, 0, 0, 0),
(19, 25, 12, 19, 0),
(21, 24, 8, 10, 0),
(22, 24, 0, 0, 0),
(23, 24, 7, 8, 0),
(24, 24, 0, 0, 0),
(25, 24, 0, 0, 0),
(26, 24, 9, 11, 0),
(27, 25, 0, 0, 0),
(28, 25, 10, 17, 0),
(29, 25, 11, 18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_master_mata_pelajaran`
--

CREATE TABLE `tabel_master_mata_pelajaran` (
  `id_master_mapel` int(10) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL,
  `id_kelas` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_master_mata_pelajaran`
--

INSERT INTO `tabel_master_mata_pelajaran` (`id_master_mapel`, `nama_mapel`, `id_kelas`) VALUES
(1, 'Matematika', 5),
(2, 'IPA', 5),
(3, 'IPS', 1),
(4, 'Agama Islam', 6),
(5, 'Bahasa Inggris', 6),
(6, 'Bahasa Inggris', 7),
(7, 'Seni Budaya', 8),
(8, 'Informatika', 7),
(9, 'IPS', 7),
(10, 'Informatika', 8),
(11, 'Informatika', 9),
(12, 'Informatika', 10),
(13, 'Informatika', 11),
(14, 'Informatika', 12),
(15, 'IPS', 8),
(16, 'IPS', 9),
(17, 'IPS', 10),
(18, 'IPS', 11),
(19, 'IPS', 12);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_materi`
--

CREATE TABLE `tabel_materi` (
  `id_materi` int(20) NOT NULL,
  `id_kelas` int(20) NOT NULL,
  `id_mapel` int(20) NOT NULL,
  `judul` text NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `original_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_materi`
--

INSERT INTO `tabel_materi` (`id_materi`, `id_kelas`, `id_mapel`, `judul`, `nama_file`, `original_name`) VALUES
(1, 5, 2, 'IPA1', 'file_1640487885546.pdf', 'Imconnect Enterprise.pdf'),
(4, 5, 2, 'IPA 2', 'file_1640493065322.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(5, 5, 1, 'Matematika 1', 'file_1640493088696.pdf', 'Tampilan Menu Simona pada admin.pdf'),
(6, 5, 1, 'Materi 1', 'file_1640497981201.pdf', 'INDIVIDU+DAN+MASYARAKAT.pdf'),
(7, 8, 7, 'Materi 1 Seni Budaya', 'file_1640843811784.pdf', '93. Seni Budaya SMP 1 S2 B9 RITA PURWANTI.docx.pdf'),
(8, 9, 11, 'Materi Pertemuan 1', 'file_1641476144583.pdf', 'materi informatika kelas7.pdf'),
(9, 7, 8, 'Materi Pertemuan 1', 'file_1641476181926.pdf', 'materi informatika kelas7.pdf'),
(10, 8, 10, 'Materi Pertemuan 1', 'file_1641476208515.pdf', 'materi informatika kelas7.pdf'),
(11, 12, 19, 'Materi 1', 'file_1641477636124.pdf', '55. IPS SMP 7 S1 B1 RITA PURWANTI.docx.pdf'),
(12, 10, 17, 'Materi 1', 'file_1641477666593.pdf', '55. IPS SMP 7 S1 B1 RITA PURWANTI.docx.pdf'),
(13, 11, 18, 'Materi 1', 'file_1641477684402.pdf', '55. IPS SMP 7 S1 B1 RITA PURWANTI.docx.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_nama_tugas`
--

CREATE TABLE `tabel_nama_tugas` (
  `id_nama_tugas` int(10) NOT NULL,
  `nama_tugas` varchar(200) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `durasi` int(20) NOT NULL,
  `status_tugas` int(11) NOT NULL,
  `id_kelas` int(20) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_nama_tugas`
--

INSERT INTO `tabel_nama_tugas` (`id_nama_tugas`, `nama_tugas`, `id_mapel`, `durasi`, `status_tugas`, `id_kelas`, `tanggal`) VALUES
(7, 'Matematika Diskrit', 1, 60, 1, 5, '2021-12-12'),
(8, 'Bahasa Inggris 1', 5, 1, 1, 6, '2021-12-19'),
(10, 'ujian 1', 1, 120, 2, 5, '2021-12-25'),
(11, 'ujian 2', 1, 20, 2, 5, '2021-12-25'),
(12, 'Tugas 2 Matematika', 1, 120, 1, 5, '2021-12-26'),
(15, 'Seni Budaya 1', 7, 60, 1, 8, '2021-12-30'),
(16, 'UTS Seni Budaya', 7, 60, 2, 8, '2021-12-30'),
(23, 'Tugas 1', 18, 20, 1, 11, '2022-01-07'),
(24, 'coba ujian', 18, 30, 2, 11, '2022-01-07'),
(25, 'Tugas 1', 19, 20, 1, 12, '2022-01-09'),
(26, 'Tugas 1', 17, 20, 1, 10, '2022-01-09'),
(27, 'Tugas 1', 10, 20, 1, 8, '2022-01-09'),
(28, 'Tugas 1', 8, 20, 1, 7, '2022-01-09'),
(29, 'Tugas 1', 11, 20, 1, 9, '2022-01-09');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_nilai`
--

CREATE TABLE `tabel_nilai` (
  `id_nilai` int(10) NOT NULL,
  `id_siswa` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_tugas` int(10) NOT NULL,
  `nilai_pg` double DEFAULT NULL,
  `waktu_mulai` varchar(200) NOT NULL,
  `waktu_selesai` varchar(200) NOT NULL,
  `nilai_keseluruhan` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_nilai`
--

INSERT INTO `tabel_nilai` (`id_nilai`, `id_siswa`, `id_kelas`, `id_mapel`, `id_tugas`, `nilai_pg`, `waktu_mulai`, `waktu_selesai`, `nilai_keseluruhan`) VALUES
(6, 3, 5, 1, 7, 20, '1639283075156', '1639283096369', 50),
(7, 4, 6, 5, 8, 10, '1639886045950', '1639886105072', 10),
(8, 3, 5, 1, 10, 30, '1640393331585', '1640396956220', 50),
(10, 3, 5, 1, 11, 30, '1640428660149', '1640428749732', 50),
(11, 3, 5, 1, 12, 0, '1640499388757', '1640499485785', 50),
(12, 4, 8, 7, 15, 1, '1640846385253', '1640846918461', 10),
(13, 4, 8, 7, 16, 10, '1640850557456', '1640850698387', 10),
(14, 5, 7, 8, 28, 80, '1641817235711', '1641817287967', NULL),
(15, 6, 7, 8, 28, 80, '1641817531738', '1641817557753', NULL),
(16, 7, 7, 8, 28, 40, '1641817631071', '1641817664364', NULL),
(17, 8, 7, 8, 28, 40, '1641818250759', '1641818289041', NULL),
(18, 10, 8, 10, 27, 20, '1641818433585', '1641818466524', NULL),
(19, 11, 8, 10, 27, 20, '1641818497453', '1641818516571', NULL),
(20, 12, 8, 10, 27, 20, '1641818552989', '1641818571305', NULL),
(21, 4, 9, 11, 29, 60, '1641818659234', '1641818677993', NULL),
(22, 16, 10, 17, 26, 60, '1641818740298', '1641818774483', NULL),
(23, 17, 10, 17, 26, 40, '1641818805888', '1641818835532', NULL),
(24, 18, 10, 17, 26, 20, '1641818873206', '1641818896826', NULL),
(25, 19, 10, 17, 26, 60, '1641819012245', '1641819030205', NULL),
(26, 20, 11, 18, 23, 20, '1641819064811', '1641819085156', NULL),
(27, 21, 11, 18, 23, 80, '1641819436329', '1641819449049', NULL),
(28, 22, 11, 18, 23, 40, '1641819472019', '1641819495285', NULL),
(29, 23, 11, 18, 23, 60, '1641819518292', '1641819531155', NULL),
(30, 24, 12, 19, 25, 80, '1641819552701', '1641819587785', NULL),
(31, 25, 12, 19, 25, 40, '1641819666502', '1641819680391', NULL),
(32, 26, 12, 19, 25, 80, '1641819703391', '1641819719213', NULL),
(33, 27, 12, 19, 25, 80, '1641819737794', '1641819750711', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_siswa`
--

CREATE TABLE `tabel_siswa` (
  `id_siswa` int(20) NOT NULL,
  `id_kelas` int(30) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `nis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_siswa`
--

INSERT INTO `tabel_siswa` (`id_siswa`, `id_kelas`, `nama_siswa`, `nis`) VALUES
(4, 9, 'ALBI YANSYA', '12065'),
(5, 7, 'Aulia Ramdhani ', '11996'),
(6, 7, 'Davina Lindia Sari ', '12001'),
(7, 7, 'Fajar Algivari ', '12005'),
(8, 7, 'Dede Hidayat ', '12002'),
(9, 8, 'Jessica Putri Patty ', ' 12046'),
(10, 8, 'Meisca Kurnia Fraida', '12050'),
(11, 8, 'Falah Al Baihaqi ', '12039'),
(12, 8, 'Muhammad Fahry ', '12051'),
(13, 9, 'Najma Putri Anjani ', '12093'),
(14, 9, 'Latiza Catiana ', '12086'),
(15, 9, 'Desta Damo ', ' 12078'),
(16, 10, 'Faizah Adha ', '12112'),
(17, 10, 'Saptinah', '12130'),
(18, 10, 'Ranga Pradana ', '12126'),
(19, 10, 'Fadhil Azis ', '12111'),
(20, 11, 'Moza Mustika ', '12158'),
(21, 11, 'Gadis Amalia ', '12147'),
(22, 11, 'Ilham Muslimin ', '12149'),
(23, 11, 'Agung Diwatra ', '12137'),
(24, 12, 'Mila Apsari ', '12192'),
(25, 12, 'Aulia Nur Zahra ', '12180'),
(26, 12, 'Muhammad Alif ', '12193'),
(27, 12, 'Irfan Gunawan ', '12187');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_soal`
--

CREATE TABLE `tabel_soal` (
  `id_soal` int(10) NOT NULL,
  `id_pelajaran` int(10) NOT NULL,
  `id_tugas` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` text DEFAULT NULL,
  `type_soal` tinyint(1) NOT NULL,
  `no_urut_soal` int(10) NOT NULL,
  `file` text DEFAULT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_soal`
--

INSERT INTO `tabel_soal` (`id_soal`, `id_pelajaran`, `id_tugas`, `id_kelas`, `soal`, `jawaban`, `type_soal`, `no_urut_soal`, `file`, `point`) VALUES
(15, 1, 7, 5, '1 ditambah 1 sama dengan ?', 'A. 2;B. 1;C. 4;D. 3', 1, 1, NULL, 0),
(16, 1, 7, 5, '10 dikali 2 sama dengan ?', 'A. 10;B. 15;C. 20;D. 30', 1, 2, NULL, 0),
(17, 1, 7, 5, '9 dibagi 3 sama dengan ', NULL, 0, 3, NULL, 0),
(18, 5, 8, 6, 'Shiva meets Lia, her old friend, on her way walking to her office. Shiva greets her.  Shiva : You are Lia, aren’t you? Hi … Lia : Hi, Shiva. Oh I am just fine. How about yourself? Shiva : I am fine, too. I am working for that office now. Lia : Really? How lucky you are!', 'A. Where do you live now?;B. How have you been?;C. Do you remember me?;D. Do I look great?', 1, 1, NULL, 0),
(19, 5, 8, 6, 'Mr. Handoyo is eager to know about Anton’s condition. He is not cheerful as usual. Mr. Handoyo : Anton, what is the matter with you? You do not look well this morning. Anton : You are right, Sir. … I think I get cold. Mr. Handoyo : You’d better take a rest at home, then.', 'A. I am okay;B. I am just fine;C. I do not feel too well this morning;D. I do not feel bad', 1, 2, NULL, 0),
(20, 5, 8, 6, 'Mrs. Rossy : Good afternoon, students. Students : Good afternoon, Ma’am. Mrs. Rossy : Well, I cannot teach you this afternoon. I have a meeting with the headmaster. Students : That is alright. We will study by ourselves, Ma’am. The underlined utterance is an expression of …', NULL, 0, 3, NULL, 0),
(21, 1, 10, 5, '1 tambah 1 sama dengan?', 'A. 1;B. 2;C. 3;D. 4', 1, 1, '', 0),
(22, 1, 10, 5, '2 tambah 1 sama dengan?', 'A. 2;B. 3;C. 4;D. 1', 1, 2, '22', 0),
(23, 1, 10, 5, '3 tambah 3 sama dengan ?', 'A. 1;B. 2;C. 4;D. 6', 1, 3, '20', 0),
(24, 1, 11, 5, 'dua dikali dua sama dengan', 'A. 4;B. 2;C. 1;D. 3', 1, 1, '', 20),
(25, 1, 11, 5, '1 tambah 1 sama dengan', NULL, 0, 2, '25', 0),
(26, 1, 11, 5, '3 dikali 1 sama dengan', NULL, 0, 3, '', 0),
(27, 1, 11, 5, '2 ditambah 2 ?', 'A. 2;B. 3;C. 4;D. 1', 1, 4, '25', 10),
(28, 1, 11, 5, '2 ditambah 1 ?', 'A. 2;B. 3;C. 1;D. 34', 1, 5, '', 20),
(29, 1, 12, 5, '', NULL, 0, 1, '29', 0),
(30, 7, 15, 8, '1. Suling merupakan  alat musik yang digunakan dengan cara?', 'A. Dipukul;B. Ditiup;C. DiGesek;D. Di petik', 1, 1, '', 1),
(33, 7, 16, 8, 'Hal yang tidak termasuk dalam pengertian seni adalah ….', NULL, 0, 2, '', 0),
(34, 7, 16, 8, 'Ungkapan gagasan atau perasaan estetis dan bermakna yang diwujudkan melalui media titik, garis, bidang, bentuk, warna, tekstur, dan gelap terang yang ditata dengan prinsip-prinsip tertentu merupakan pengertian dari ....', 'A. seni musik;B. seni kerajinan;C. seni rupa;D. seni kriya', 1, 1, '', 10),
(35, 6, 17, 8, 'Semua peralatan fisik dari sistem komputer disebut …', 'A. Hardware;B. Software;C. Brainware;D. Memory', 1, 1, '', 10),
(36, 6, 17, 8, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware;D. Memory Card', 1, 2, '', 10),
(37, 6, 17, 8, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', '', 1, 3, '', 10),
(38, 6, 17, 8, 'Bagian keyboard berupa tombol angka seperti kalkulator disebut …', 'A. Typing Keys     ;B. Function Key;C. Cursor Keys     ;D. Numeric Keys', 1, 4, '', 10),
(39, 6, 17, 8, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet  Untuk chatting tatap muka disebut dengan istilah ….', NULL, 0, 5, '', 0),
(40, 6, 18, 7, 'Semua peralatan fisik dari sistem komputer disebut …', '', 1, 1, '', 10),
(41, 6, 18, 7, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware;D. Memory Card', 1, 2, '', 10),
(42, 6, 18, 7, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', 'A. Hardisk ;B. Keyboard     ;C. Motherboard     ;D. Processor ', 1, 3, '', 10),
(43, 6, 18, 7, 'Bagian keyboard berupa tombol angka seperti kalkulator disebut …', 'A. Typing Keys     ;B. Function Key;C. Cursor Keys     ;D. Numeric Keys', 1, 4, '', 10),
(44, 6, 18, 7, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet  Untuk chatting tatap muka disebut dengan istilah ….', NULL, 0, 5, '', 0),
(45, 6, 19, 9, 'Semua peralatan fisik dari sistem komputer disebut …', 'A. Hardware     ;B. Software;C. Brainware     ;D. Memory', 1, 1, '', 10),
(46, 6, 19, 9, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware  ;D. Memory Card', 1, 2, '', 10),
(47, 6, 19, 9, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', '', 1, 3, '', 10),
(48, 6, 19, 9, 'Bagian keyboard berupa tombol angka seperti kalkulator disebut …', '', 1, 4, '', 10),
(49, 6, 19, 9, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet  Untuk chatting tatap muka disebut dengan istilah ….', NULL, 0, 5, '', 0),
(50, 15, 20, 12, 'Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 10),
(51, 15, 20, 12, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', '', 1, 2, '', 10),
(52, 15, 20, 12, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', '', 1, 3, '', 10),
(53, 15, 20, 12, 'Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', '', 1, 4, '', 10),
(54, 15, 20, 12, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(55, 15, 21, 10, 'Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 10),
(56, 15, 21, 10, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', 'A. Bukit;B. Plato;C. Gunung;D. Palung laut', 1, 2, '', 10),
(57, 15, 21, 10, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', 'A. Animinisme;B. Dinamisme;C. Komunisme;D. Totemisme', 1, 3, '', 10),
(58, 15, 21, 10, 'Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', '', 1, 4, '', 10),
(59, 15, 21, 10, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(60, 15, 22, 11, '. Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 10),
(61, 15, 22, 11, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', 'A. Bukit;B. Plato;C. Gunung;D. Palung laut', 1, 2, '', 10),
(62, 15, 22, 11, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', '', 1, 3, '', 10),
(63, 15, 22, 11, 'Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', '', 1, 4, '', 10),
(64, 15, 22, 11, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(67, 18, 24, 11, 'nomor 1', 'A. coba;B. tidak dicoba', 1, 1, '35', 20),
(68, 19, 25, 12, 'Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 20),
(69, 19, 25, 12, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', 'A. Bukit;B. Plato;C. Gunung;D. Palung laut', 1, 2, '', 20),
(70, 19, 25, 12, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', 'A. Animinisme;B. Dinamisme;C. Komunisme;D. Totemisme', 1, 3, '', 20),
(71, 19, 25, 12, 'Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', 'A. Kuarter;B. Primer;C. Sekunder;D. Palaezoikum', 1, 4, '', 20),
(72, 19, 25, 12, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(73, 17, 26, 10, 'Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 20),
(74, 17, 26, 10, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', 'A. Bukit;B. Plato;C. Gunung;D. Palung laut', 1, 2, '', 20),
(75, 17, 26, 10, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', 'A. Animinisme;B. Dinamisme;C. Komunisme;D. Totemisme', 1, 3, '', 20),
(76, 17, 26, 10, '. Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', '', 1, 4, '', 20),
(77, 17, 26, 10, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(78, 18, 23, 11, 'Gempa bumi dan tsunami hebat yang terjadi di Aceh 26 Desember 2004 disebabkan oleh aktivitas…', 'A. Tektonik;B. Vulkanik;C. Tektovulkanik;D. Gempa', 1, 1, '', 20),
(79, 18, 23, 11, 'Berikut ini merupakan bentuk muka bumi di darat, kecuali…', 'A. Bukit;B. Plato;C. Gunung;D. Palung laut', 1, 2, '', 20),
(80, 18, 23, 11, 'Kepercayaan yang meyakini bahwa hewan tertentu memiliki kekuatan dan dianggap suci disebut…', 'A. Animinisme;B. Dinamisme;C. Komunisme;D. Totemisme', 1, 3, '', 20),
(81, 18, 23, 11, 'Zaman di mana pertama kalinya terdapat kehidupan makhluk hidup adalah pada zaman…', 'A. Kuarter;B. Primer;C. Sekunder;D. Palaezoikum', 1, 4, '', 20),
(82, 18, 23, 11, 'Pembagian kerja di kalangan manusia purba pada masa berburu didasarkan pada…', NULL, 0, 5, '', 0),
(83, 10, 27, 8, 'Semua peralatan fisik dari sistem komputer disebut …', '', 1, 1, '', 20),
(84, 10, 27, 8, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware;D. Memory Card', 1, 2, '', 20),
(85, 10, 27, 8, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', '', 1, 3, '', 20),
(86, 10, 27, 8, '. Bagian keyboard berupa tombol angka seperti kalkulator disebut …', '', 1, 4, '', 20),
(87, 10, 27, 8, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet ', NULL, 0, 5, '', 0),
(88, 8, 28, 7, 'Semua peralatan fisik dari sistem komputer disebut …', 'A. Hardware     ;B. Software;C. Brainware     ;D. Memory', 1, 1, '', 20),
(89, 8, 28, 7, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware  ;D. Memory Card', 1, 2, '', 20),
(90, 8, 28, 7, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', 'A. Hardisk ;B. Keyboard     ;C. Motherboard     ;D. Processor ', 1, 3, '', 20),
(91, 8, 28, 7, 'Bagian keyboard berupa tombol angka seperti kalkulator disebut …', 'A. Typing Keys     ;B. Function Key;C. Cursor Keys     ;D. Numeric Keys', 1, 4, '', 20),
(92, 8, 28, 7, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet ', NULL, 0, 5, '', 0),
(93, 11, 29, 9, 'Semua peralatan fisik dari sistem komputer disebut …', 'A. Hardware     ;B. Software;C. Brainware     ;D. Memory', 1, 1, '', 20),
(94, 11, 29, 9, 'Sistem operasi dan aplikasi dari suatu komputer disebut dengan istilah…', 'A. Perangkat Keras ;B. Perangkat Lunak    ;C. Brainware  ;D. Memory Card', 1, 2, '', 20),
(95, 11, 29, 9, 'Perangkat keras komputer yang disebut sebagai otaknya komputer disebut ….', 'A. Hardisk ;B. Keyboard     ;C. Motherboard     ;D. Processor ', 1, 3, '', 20),
(96, 11, 29, 9, 'Bagian keyboard berupa tombol angka seperti kalkulator disebut …', 'A. Keys     Typing ;B. Function Key;C. Cursor Keys     ;D. Numeric Keys', 1, 4, '', 20),
(97, 11, 29, 9, 'Kamera kecil yang diletakkan di atas monitor. Kamera tersebut dapat digunakan pengguna internet ', NULL, 0, 5, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_tu`
--

CREATE TABLE `tabel_tu` (
  `id_tu` int(2) NOT NULL,
  `nama_tu` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_tu`
--

INSERT INTO `tabel_tu` (`id_tu`, `nama_tu`, `nip`) VALUES
(1, 'admin', ''),
(4, 'adminmaster', '8973423427732');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `status_materi`
--
ALTER TABLE `status_materi`
  ADD PRIMARY KEY (`id_status_materi`);

--
-- Indexes for table `status_tugas`
--
ALTER TABLE `status_tugas`
  ADD PRIMARY KEY (`id_status_tugas`);

--
-- Indexes for table `tabel_access_token`
--
ALTER TABLE `tabel_access_token`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD UNIQUE KEY `identitas_pengguna` (`identitas_pengguna`);

--
-- Indexes for table `tabel_file`
--
ALTER TABLE `tabel_file`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `tabel_guru`
--
ALTER TABLE `tabel_guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `tabel_jawaban_siswa`
--
ALTER TABLE `tabel_jawaban_siswa`
  ADD PRIMARY KEY (`id_jawaban_siswa`);

--
-- Indexes for table `tabel_kelas`
--
ALTER TABLE `tabel_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tabel_kunci_jawaban`
--
ALTER TABLE `tabel_kunci_jawaban`
  ADD PRIMARY KEY (`id_kunci_jawaban`);

--
-- Indexes for table `tabel_list_kelas_guru`
--
ALTER TABLE `tabel_list_kelas_guru`
  ADD PRIMARY KEY (`id_list_kelas`);

--
-- Indexes for table `tabel_master_mata_pelajaran`
--
ALTER TABLE `tabel_master_mata_pelajaran`
  ADD PRIMARY KEY (`id_master_mapel`);

--
-- Indexes for table `tabel_materi`
--
ALTER TABLE `tabel_materi`
  ADD PRIMARY KEY (`id_materi`);

--
-- Indexes for table `tabel_nama_tugas`
--
ALTER TABLE `tabel_nama_tugas`
  ADD PRIMARY KEY (`id_nama_tugas`);

--
-- Indexes for table `tabel_nilai`
--
ALTER TABLE `tabel_nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `tabel_siswa`
--
ALTER TABLE `tabel_siswa`
  ADD PRIMARY KEY (`id_siswa`),
  ADD UNIQUE KEY `nis` (`nis`);

--
-- Indexes for table `tabel_soal`
--
ALTER TABLE `tabel_soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tabel_tu`
--
ALTER TABLE `tabel_tu`
  ADD PRIMARY KEY (`id_tu`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `status_materi`
--
ALTER TABLE `status_materi`
  MODIFY `id_status_materi` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_tugas`
--
ALTER TABLE `status_tugas`
  MODIFY `id_status_tugas` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `tabel_access_token`
--
ALTER TABLE `tabel_access_token`
  MODIFY `id_token` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  MODIFY `id_akun` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tabel_file`
--
ALTER TABLE `tabel_file`
  MODIFY `id_file` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tabel_guru`
--
ALTER TABLE `tabel_guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tabel_jawaban_siswa`
--
ALTER TABLE `tabel_jawaban_siswa`
  MODIFY `id_jawaban_siswa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `tabel_kelas`
--
ALTER TABLE `tabel_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tabel_kunci_jawaban`
--
ALTER TABLE `tabel_kunci_jawaban`
  MODIFY `id_kunci_jawaban` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `tabel_list_kelas_guru`
--
ALTER TABLE `tabel_list_kelas_guru`
  MODIFY `id_list_kelas` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tabel_master_mata_pelajaran`
--
ALTER TABLE `tabel_master_mata_pelajaran`
  MODIFY `id_master_mapel` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tabel_materi`
--
ALTER TABLE `tabel_materi`
  MODIFY `id_materi` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tabel_nama_tugas`
--
ALTER TABLE `tabel_nama_tugas`
  MODIFY `id_nama_tugas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tabel_nilai`
--
ALTER TABLE `tabel_nilai`
  MODIFY `id_nilai` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tabel_siswa`
--
ALTER TABLE `tabel_siswa`
  MODIFY `id_siswa` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tabel_soal`
--
ALTER TABLE `tabel_soal`
  MODIFY `id_soal` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tabel_tu`
--
ALTER TABLE `tabel_tu`
  MODIFY `id_tu` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
