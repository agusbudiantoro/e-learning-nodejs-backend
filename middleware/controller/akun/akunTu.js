const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_guru = require('../../model/tabel_guru');
const tabel_akun = require('../../model/tabel_akun');
const tabel_siswa = require('../../model/tabel_siswa/tabel_siswa');
const tabel_tu = require('../../model/tabel_tu');
const tabel_access_token = require('../../model/tabel_access_token');

exports.registerAdminTu = async function (req, res) {
    var post = {
        nama_tu:req.body.nama_tu,
        nip:req.body.nip,
        // foto: `${req.file.fieldname}_${Date.now()}${path.extname(req.file.originalname)}`
    }
    let password=req.body.password;
    let resDataTu='';
    let resPromiseDataTu='';
    try {
        resDataTu = await tabel_tu.getDataTu(res,post); 
        resPromiseDataTu = await resDataTu;  
    } catch (error) {
        res.status(404).send(error);
    
    }
    if(resPromiseDataTu.length == 0){
        let resInsertTu = await tabel_tu.insertDataTu(res,post);
        let resPromiseInsertTu = await resInsertTu;

        let resGetAkunTu = await tabel_akun.getDataAkun(res,post,1);
        let resPromiseGetAkunTu = await resGetAkunTu;
        let idPengguna = await resPromiseInsertTu.insertId
        if(resPromiseGetAkunTu.length == 0){
            return tabel_akun.registerAkun(res,post,1, password, idPengguna);
        } else{ 
            return tabel_akun.updateIdAkun(res,post,1,password,idPengguna)
        }
    } else {
        res.status(405).send("data sudah ada");
    }
}