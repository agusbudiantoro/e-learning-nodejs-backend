const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.deleteModelKelas = async function(res,id){
    return new Promise(function(resolve,reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_kelas","id_kelas",id];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                res.status(200).send("berhasil menghapus kelas");
            }
        });
    });
}