
const tabel_mapel = require('../../model/tabel_mapel/put');

exports.editMapel = async function (req, res) {
        let body = {
            id_master_mapel:req.params.id_master_mapel,
            nama_mapel:req.body.nama_mapel,
            id_kelas:req.body.id_kelas
        }

        await tabel_mapel.putModelMapel(res,body);
}
