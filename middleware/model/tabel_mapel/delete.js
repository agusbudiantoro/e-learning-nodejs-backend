const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.deleteModelMapel = async function(res,id){
    return new Promise(function(resolve,reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_master_mata_pelajaran","id_master_mapel",id];
        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil hapus");
            }
        });
    });
}