const connection = require('../../../../koneksi');
const mysql = require('mysql');

exports.getModelKunciByIdSoal = async function(res,data){
    return new Promise(function (resolve, reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_kunci_jawaban", "id_soal", data.id_soal];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                res.status(200).json(rows)
            }
        });
    });
}