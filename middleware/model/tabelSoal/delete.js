const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.deleteModelSoal = async function(res,post){
    return new Promise(function(resolve,reject){
        let id_soal = post.id_soal;
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_soal","id_soal",post.id_soal];
        query = mysql.format(query, table);
        console.log(post);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                var query1 = "DELETE FROM ?? WHERE ??=?";
                var table1 = ["tabel_kunci_jawaban","id_soal",id_soal];
                query1 = mysql.format(query1, table1);
                console.log(post);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        res.status(404).send(error);
                    } else {
                        res.status(200).send("berhasil menghapus soal");
                    }
                });
            }
        });
    });
}