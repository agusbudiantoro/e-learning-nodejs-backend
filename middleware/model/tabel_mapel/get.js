const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getDataMapelByidKelas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_master_mata_pelajaran", "id_kelas", post.id_kelas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getAllDataMapel = async function (res){
    return new Promise(function(resolve,reject){
        var query = "SELECT `tabel_master_mata_pelajaran`.`id_master_mapel`, `tabel_master_mata_pelajaran`.`nama_mapel`, `tabel_master_mata_pelajaran`.`id_kelas`, `tabel_kelas`.`nama_kelas` FROM `tabel_master_mata_pelajaran` INNER JOIN `tabel_kelas` ON `tabel_kelas`.`id_kelas`=`tabel_master_mata_pelajaran`.`id_kelas`"

        query = mysql.format(query);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};