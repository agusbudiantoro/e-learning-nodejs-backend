const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getListKelasByIdGuru = async function (res,id_guru){
    return new Promise(function(resolve,reject){
        var query = "SELECT a.id_guru, a.id_list_kelas, a.id_kelas, a.id_mapel, a.status, c.nama_kelas, d.nama_mapel FROM tabel_list_kelas_guru AS a INNER JOIN tabel_kelas AS c ON a.id_kelas=c.id_kelas INNER JOIN tabel_master_mata_pelajaran AS d ON a.id_mapel=d.id_master_mapel WHERE a.id_guru=?"
        var table = [id_guru];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getListKelasByIdGuruForGuru = async function (res,id_guru){
    return new Promise(function(resolve,reject){
        var query = "SELECT DISTINCT a.id_kelas, a.id_guru, c.nama_kelas FROM tabel_list_kelas_guru AS a INNER JOIN tabel_kelas AS c ON a.id_kelas=c.id_kelas WHERE a.id_guru=?"
        var table = [id_guru];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getListMapelKelasByIdKelas = async function (res,id_guru,id_kelas){
    return new Promise(function(resolve,reject){
        var query = "SELECT a.id_guru, a.id_list_kelas, a.id_kelas, a.id_mapel, a.status, c.nama_kelas, d.nama_mapel FROM tabel_list_kelas_guru AS a INNER JOIN tabel_kelas AS c ON a.id_kelas=c.id_kelas INNER JOIN tabel_master_mata_pelajaran AS d ON a.id_mapel=d.id_master_mapel WHERE a.id_guru=? AND a.id_kelas=?"
        var table = [id_guru,id_kelas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};