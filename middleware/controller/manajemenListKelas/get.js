const getListKelas = require('../../model/tabel_listKelasGuru/get');

exports.getListKelasByIdGuru = async function (req, res) {
    await getListKelas.getListKelasByIdGuru(res, req.params.id_guru);
}

exports.getListMapelKelasByIdKelas = async function (req, res) {
    await getListKelas.getListMapelKelasByIdKelas(res, req.params.id_guru,req.params.id_kelas);
}

exports.getListKelasByIdGuruForGuru = async function (req, res) {
    await getListKelas.getListKelasByIdGuruForGuru(res, req.params.id_guru);
}