const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postModelFile = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_file"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).json({
                    message:"berhasil menambahkan file",
                    id:rows.insertId
                });
            }
        });
    });
}