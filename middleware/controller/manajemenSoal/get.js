
const tabel_soal = require('../../model/tabelSoal/get');

exports.getSoalByIdTugas = async function (req, res) {
        let body = {
            id_pelajaran:req.body.id_pelajaran,
            id_kelas:req.params.id_kelas,
            id_tugas:req.params.id_tugas,
        }
        // jika type soal 1 atau true maka pilihan ganda
        // jika type soal 0 atau false maka essay
        let response = await tabel_soal.getModelSoalByIdTugas(res,body);
}
