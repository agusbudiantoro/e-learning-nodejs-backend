const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putNilaiKeseluruhan = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE tabel_nilai SET nilai_keseluruhan=? WHERE id_siswa=? AND id_tugas"
        var table = [post.nilai_keseluruhan,post.id_siswa,post.id_tugas];
        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan"+error);
            } else {
               res.status(200).send("berhasil memberi nilai keseluruhan")
            }
        });
    })
};