
const tabel_tugas = require('../../model/tabel_tugas/post');

exports.tambahTugas = async function (req, res) {
    console.log(req.body);
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
        let body = {
            nama_tugas:req.body.nama_tugas,
            id_mapel:req.body.id_mapel,
            durasi:req.body.durasi,
            status_tugas:req.body.status_tugas,
            id_kelas:req.body.id_kelas,
            tanggal:String(year)+"/"+String(month)+"/"+String(day)
        }

        await tabel_tugas.postModelTugas(res,body);
}
