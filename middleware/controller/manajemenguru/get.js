const tabel_guru = require('../../model/tabel_guru')

exports.getAllDataGuru = async function(req,res){
        let tampungData=[]
        let resAkunGuru=[];
        let resGuru = await tabel_guru.getAllDataGuru();
        for(var i=0; i<resGuru.length;i++){
            await tampungData.push({
                id_guru: resGuru[i].id_guru,
                no_urut_guru: resGuru[i].no_urut_guru,
                nip: resGuru[i].nip,
                nama: resGuru[i].nama,
                tempat_tgl_lahir: resGuru[i].tempat_tgl_lahir,
                status_wali_kelas: resGuru[i].status_wali_kelas,
                no_hp: resGuru[i].no_hp,
                alamat_rumah: resGuru[i].alamat_rumah,
                pangkat_golongan: resGuru[i].pangkat_golongan,
                tmt_cpns: resGuru[i].tmt_cpns,
                tmt_unit_kerja: resGuru[i].tmt_unit_kerja,
                pendidikan: resGuru[i].pendidikan,
                jurusan: resGuru[i].jurusan,
                status: resGuru[i].status,
                status_guru: resGuru[i].status_guru,
                jabatan: resGuru[i].jabatan,
                id_mapel: resGuru[i].id_mapel,
                id_kelas:resGuru[i].id_kelas,
                akun:{}
            })
            resAkunGuru = await tabel_guru.getDataAkunGuru(res,resGuru[i].nip);
            tampungData[i].akun= await resAkunGuru[0];
        }

        res.status(200).json(tampungData);
}