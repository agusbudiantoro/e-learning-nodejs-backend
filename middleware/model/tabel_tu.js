const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.getDataTu = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_tu", "nip", post.nip];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.insertDataTu = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_tu"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}