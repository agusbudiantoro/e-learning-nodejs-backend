const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.getDataGuru = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_guru", "nip", post.nip];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getAllDataGuru = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ??"
        var table = ["tabel_guru"];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getDataAkunGuru = async function (res,id){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "identitas_pengguna", id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.insertDataGuru = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_guru"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.updateDataGuru = async function(res,post){
    return new Promise(async function(resolve,reject){
            var query = "";
            var table=[];
            console.log(post.jurusan);
                query = await "UPDATE tabel_guru SET no_urut_guru=?, nip=?, nama=?, tempat_tgl_lahir=?, status_wali_kelas=?, no_hp=?, alamat_rumah=?, pangkat_golongan=?, tmt_cpns=?, tmt_unit_kerja=?, pendidikan=?, jurusan=?, status=?, status_guru=?, jabatan=? WHERE id_guru=?";
                table = await [post.no_urut_guru, post.nip,post.nama,post.tempat_tgl_lahir,post.status_wali_kelas, post.no_hp,post.alamat_rumah,post.pangkat_golongan,post.tmt_cpns.substring(0,10),post.tmt_unit_kerja.substring(0,10),post.pendidikan,post.jurusan,post.status,post.status_guru,post.jabatan, post.id_pengguna];
            
            query = mysql.format(query, table);
            console.log(query);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update guru",value:error })
                } else {
                    resolve(rows);
                }
            });
        });
}

exports.delGuru= async function(res,id){
    return new Promise(function(resolve, reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_guru","id_guru",id];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                res.status(200).send({ message: "berhasil hapus data dan akun guru" })
            }
        });
    });
}