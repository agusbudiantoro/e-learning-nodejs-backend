
const tabel_nilai = require('../../model/tabel_nilai/post');
const tabel_kunci = require('../../model/tabel_kunci_jawaban/get');
const tabel_soal = require('../../model/tabelSoal/get');

exports.tambahNilai = async function (req, res) {
    console.log(req.body);
    console.log("lihat");
        let nilai="";
        for(var i=0;i<req.body.jawaban_siswa.length;i++){
            if(req.body.jawaban_siswa[i].type_soal == 1){
                let resKunci = await tabel_kunci.getKunciByIdSoal(res,req.body.jawaban_siswa[i].id_soal);
                let resPointSoal = await tabel_soal.getModelSoalByIdSoal(res,req.body.jawaban_siswa[i]);
                console.log(resPointSoal);
                console.log(resKunci);
                if(resKunci[0].jawaban == req.body.jawaban_siswa[i].jawaban){
                    nilai=Number(nilai)+Number(resPointSoal[0].point)
                } else {
                    nilai= Number(nilai)+0;
                }
            }
        }
        console.log(nilai);

        let body =await {
            id_siswa:req.body.id_siswa,
            id_kelas:req.body.id_kelas,
            id_mapel:req.body.id_mapel,
            id_tugas:req.body.id_tugas,
            nilai_pg:nilai,
            waktu_mulai:req.body.waktu_mulai,
            waktu_selesai:req.body.waktu_selesai
        }

        await tabel_nilai.postModelNilai(res,body);
}
