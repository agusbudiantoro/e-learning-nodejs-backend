const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getKunciByIdSoal = async function (res,id){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_kunci_jawaban", "id_soal", id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                resolve(rows);
            }
        });
    })
};