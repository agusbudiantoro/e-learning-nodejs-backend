const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getTugasByIdMapel = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?"
        var table = ["tabel_nama_tugas", "id_mapel", post.id_mapel, "status_tugas",post.status];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getTugasByIdTugas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_nama_tugas", "id_nama_tugas", post.id_nama_tugas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.getTugasByIdKelas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=?"
        var table = ["tabel_nama_tugas", "id_kelas", post.id_kelas, "status_tugas", post.status_tugas,"id_mapel",post.id_mapel];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows);
            }
        });
    })
};