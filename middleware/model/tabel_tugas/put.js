const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putTabelTugas = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE tabel_nama_tugas SET nama_tugas=?, durasi=? WHERE id_nama_tugas=?"
        var table = [post.nama_tugas,post.durasi,post.id_nama_tugas];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan"+error);
            } else {
                res.status(200).send("berhasil put data");
            }
        });
    })
};