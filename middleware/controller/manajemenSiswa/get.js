const tabel_siswa = require('../../model/tabel_siswa/tabel_siswa')

exports.getAllDataSiswa = async function(req,res){
        let tampungData=[]
        let resAkunSiswa=[];
        let resSiswa = await tabel_siswa.getAllDataSiswa();
        for(var i=0; i<resSiswa.length;i++){
            await tampungData.push({
                id_siswa: resSiswa[i].id_siswa,
                nis: resSiswa[i].nis,
                nama_siswa: resSiswa[i].nama_siswa,
                id_kelas:resSiswa[i].id_kelas,
                akun:{}
            })
            resAkunSiswa = await tabel_siswa.getDataAkunSiswa(res,resSiswa[i].nis);
            tampungData[i].akun= await resAkunSiswa[0];
        }

        res.status(200).json(tampungData);
}