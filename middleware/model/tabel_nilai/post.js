const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.postModelNilai = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_nilai"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menambahkan nilai");
            }
        });
    });
}