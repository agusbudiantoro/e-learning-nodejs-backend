const tabel_akun = require('../../model/tabel_akun');
const tabel_guru = require('../../model/tabel_guru');
const tabel_siswa = require('../../model/tabel_siswa/tabel_siswa');

exports.deleteAkun = async function(req,res){
    let id_akun =await req.params.id_akun;
    let tampungData = {};
    let resGetData = await tabel_akun.getDataAkunByIdAkun(res,id_akun);
    if(resGetData.length != 0){
        tampungData = await resGetData[0];
        let resDelAkun = await tabel_akun.delAkun(res,id_akun);
        console.log(tampungData);
        if(tampungData.role == 2){
            await tabel_guru.delGuru(res,tampungData.id_pengguna);
        } else if(tampungData.role == 3){
            await tabel_siswa.delSiswa(res,tampungData.id_pengguna);
        }
    }
    
}