const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.deleteModelTugas = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_nama_tugas","id_nama_tugas",post.id_nama_tugas];
        query = mysql.format(query, table);
        console.log(post);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                res.status(200).send("berhasil menghapus tugas");
            }
        });
    });
}