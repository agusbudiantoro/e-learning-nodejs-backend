const connection = require('../../../../koneksi');
const mysql = require('mysql');

exports.getStatusTugasByIdTugasAndIdSiswa = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?"
        var table = ["status_tugas", "id_tugas", post.id_tugas,"id_siswa",post.id_siswa];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                resolve(rows);
            }
        });
    })
};