const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getModelSoalByIdTugas = async function(res,data){
    return new Promise(function (resolve, reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_soal", "id_tugas", data.id_tugas];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                res.status(200).json(rows)
            }
        });
    });
}

exports.getModelSoalByIdSoal = async function(res,data){
    console.log(data);
    return new Promise(function (resolve, reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_soal", "id_soal", data.id_soal];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}