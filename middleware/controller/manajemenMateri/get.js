
const tabel_materi = require('../../model/tabel_materi/get');

exports.getMateriByIdKelasIdMapel = async function (req, res) {
   
        let body = {
            id_kelas:req.params.id_kelas,
            id_mapel:req.params.id_mapel
        }
        await tabel_materi.getModelMateri(res,body);
}
