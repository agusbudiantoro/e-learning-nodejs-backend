
const tabel_status_tugas = require('../../../model/tabel_tugas/tabel_status_tugas/post');
const puttabel_status_tugas = require('../../../model/tabel_tugas/tabel_status_tugas/put');
const get_tabel_statusTugas = require('../../../model/tabel_tugas/tabel_status_tugas/get');

exports.tambahStatusTugas = async function (req, res) {
    console.log(req.body);
        let body = {
            id_siswa:req.body.id_siswa,
            id_tugas:req.body.id_tugas,
            waktu_mulai:req.body.waktu_mulai,
            status:req.body.status,
        }
        let response = await get_tabel_statusTugas.getStatusTugasByIdTugasAndIdSiswa(res,body);
        if(response.length == 0){
            await tabel_status_tugas.postModelStatusTugas(res,body);
        } else if(response.length == 1){
            await puttabel_status_tugas.putTabelStatusTugas(res,response[0],req.body.waktu_selesai,req.body.status);
        } else {
            res.status(403).send("gagal menambahkan data status tugas, karena status tugas dengan id tugas tersebut sudah tersedia,"+"id tugas ="+response[0].id_status_tugas);
        }
}
