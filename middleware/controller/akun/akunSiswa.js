const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_akun = require('../../model/tabel_akun');
const tabel_siswa = require('../../model/tabel_siswa/tabel_siswa');

exports.registerSiswa = async function (req, res) {
    var post = {
        id_kelas:req.body.id_kelas,
        nama_siswa:req.body.nama_siswa,
        nis:req.body.nis,
        // foto: `${req.file.fieldname}_${Date.now()}${path.extname(req.file.originalname)}`
    }
    let password=req.body.password;
    let resDataSiswa='';
    let resPromiseDataSiswa='';
    try {
        resDataSiswa = await tabel_siswa.getDataSiswa(res,post); 
        resPromiseDataSiswa = await resDataSiswa;  
    } catch (error) {
        res.status(404).send(error);
    
    }
    if(resPromiseDataSiswa.length == 0){
        let resInsertSiswa = await tabel_siswa.insertDataSiswa(res,post);
        let resPromiseInsertSiswa = await resInsertSiswa;

        let resGetAkunSiswa = await tabel_akun.getDataAkun(res,post,3);
        let resPromiseGetAkunSiswa = await resGetAkunSiswa;
        let idPengguna = await resPromiseInsertSiswa.insertId
        if(resPromiseGetAkunSiswa.length == 0){
            return tabel_akun.registerAkun(res,post,3, password, idPengguna);
        } else{ 
            return tabel_akun.updateIdAkun(res,post,3,password,idPengguna)
        }
    } else {
        res.status(405).send("data sudah ada");
    }
}

exports.editSiswa = async function (req, res) {
    var post = {
        id_pengguna:req.params.id_pengguna,
        nis:req.body.nis,
        nama_siswa:req.body.nama_siswa,
        id_kelas:req.body.id_kelas
    }
    let password=req.body.password;
    let resDataSiswa='';
    let idAkun = await req.params.id_akun;
    try {
        resDataSiswa = await tabel_siswa.updateDataSiswa(res,post);
        let dataAkun =await{
            id_akun:idAkun,
            password:password,
            identitas_pengguna:post.nis
        }
        await tabel_akun.updateAkun(res,dataAkun);
    } catch (error) {
        res.status(404).send(error);
    }
}