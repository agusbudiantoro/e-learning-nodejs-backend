const express = require('express');
const multer = require("multer");
const path = require("path");
const router = express.Router();
const sequilezeSiswa = require('./controller/manajemenSiswa/controller/index');

const auth = require('./controller/auth');
const authVerif = require('./verifikasi');

//materi
const tabelMateri = require('./controller/manajemenMateri/post');
const tabelMateriGet = require('./controller/manajemenMateri/get');
const tabelMateriPut = require('./controller/manajemenMateri/put');
const tabelMateriDelete = require('./controller/manajemenMateri/delete');

// kelas
const tabelKelas = require('./controller/manajemenKelas/post');
const tabelGetKelas = require('./controller/manajemenKelas/get');
const tabelPutKelas = require('./controller/manajemenKelas/put');
const tabelDeleteKelas = require('./controller/manajemenKelas/delete');

// mapel
const postTabelMapel = require('./controller/manajemenmapel/post');
const getTabelMapel = require('./controller/manajemenmapel/get');
const putTabelMapel = require('./controller/manajemenmapel/put');
const deleteTabelMapel = require('./controller/manajemenmapel/delete');

// tugas
const postTabelTugas = require('./controller/manajemenTugas/post');
const getTabelTugas = require('./controller/manajemenTugas/get');
const putTabelTugas = require('./controller/manajemenTugas/put');
const deleteTabelTugas = require('./controller/manajemenTugas/delete');

// soal
const post_soal = require('./controller/manajemenSoal/post');
const get_soal = require('./controller/manajemenSoal/get');
const put_soal = require('./controller/manajemenSoal/put');
const delete_soal = require('./controller/manajemenSoal/delete');

// kunci jawaban
const get_kunci = require('./controller/manajemenSoal/manajemenKunciJawaban/get');

// nilai
const post_nilai = require('./controller/manajemenNilai/post');
const get_nilai = require('./controller/manajemenNilai/get');
const get_nilai_idsiswa_idtugas = require('./controller/manajemenNilai/get');
const put_nilai_keseluruhan = require('./controller/manajemenNilai/put');

// jawaban user
const inputjawabanUser = require('./controller/manajemenJawabanUser/inputJawaban');
const getjawabanUser = require('./controller/manajemenJawabanUser/getJawaban');

// status tugas
const postStatusTugas = require('./controller/manajemenTugas/manajemenStatusTugas/post');
const getStatusTugas = require('./controller/manajemenTugas/manajemenStatusTugas/get');

//akun guru
const registerAkunGuru = require('./controller/akun/akunGuru');
const registerAkunSiswa = require('./controller/akun/akunSiswa');
const registerAkunTu = require('./controller/akun/akunTu');
const getDataGuru = require('./controller/manajemenguru/get');

// data siswa
const getDataSiswa = require('./controller/manajemenSiswa/get');
const putDataSiswa = require('./controller/akun/akunSiswa')

// del akun
const deleteAkun = require('./controller/akun/deleteAkun');

// tabel file
const postUploadFile = require('./controller/manajemenFile/post');
const deleteUploadFile = require('./controller/manajemenFile/deleteFile');
const getFile = require('./controller/manajemenFile/getFile');

//tabel list kelas guru
const postListKelasGuru = require('./controller/manajemenListKelas/post');
const getListKelasGuru = require('./controller/manajemenListKelas/get');
const putListKelasGuru = require('./controller/manajemenListKelas/put');
const deleteListKelasGuru = require('./controller/manajemenListKelas/delete');

const getGuru = require('./controller/manajemenguru/get');

router.use(express.static('public'));

const storage = multer.diskStorage({
    destination: './upload/file',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 10000000 }
})

router.post('/api/v1/registerGuru', authVerif.verifikasiAdminTu,registerAkunGuru.registerGuru);
router.post('/api/v1/registerSiswa', registerAkunSiswa.registerSiswa);
router.put('/api/v1/changePassword/:id_akun', authVerif.verifikasiGeneral,registerAkunGuru.changePassword);
router.post('/api/v1/registerTu', registerAkunTu.registerAdminTu);
router.post('/api/v1/login', auth.login);
router.delete('/api/v1/delAkun/:id_akun',authVerif.verifikasiAdminTu, deleteAkun.deleteAkun);

//materi
router.post('/api/v1/tambahMateri',authVerif.verifikasiGuru,upload.single('file'),tabelMateri.tambahMateri);
router.get('/api/v1/getMateriByIdKelasIdMapel/:id_kelas/:id_mapel',authVerif.verifikasiGeneral,tabelMateriGet.getMateriByIdKelasIdMapel);
router.put('/api/v1/editMateri',authVerif.verifikasiGeneral,upload.single('file'),tabelMateriPut.updateMateri);
router.delete('/api/v1/deleteMateri/:id_materi',authVerif.verifikasiGeneral,tabelMateriDelete.deleteMateri);

//guru
router.get('/api/v1/getAllDataGuru',authVerif.verifikasiGuru, getDataGuru.getAllDataGuru);
router.put('/api/v1/putGuru/:id_pengguna/:id_akun', authVerif.verifikasiAdminTu, registerAkunGuru.editGuru);

// tabel kelas
router.post('/api/v1/tambahKelas',authVerif.verifikasiAdminTu,tabelKelas.tambahKelas);
router.get('/api/v1/getKelasByIdMapel/:id_mapel',authVerif.verifikasiAdminAtauGuru,tabelGetKelas.getStatusTugas);
router.get('/api/v1/getAllKelas',authVerif.verifikasiAdminTu,tabelGetKelas.getKelas);
router.put('/api/v1/putKelas/:id_kelas',authVerif.verifikasiAdminTu,tabelPutKelas.putKelas);
router.delete('/api/v1/deleteKelas/:id_kelas',authVerif.verifikasiAdminTu,tabelDeleteKelas.deleteKelas);

//tabel mapel
router.post('/api/v1/tambahMapel',authVerif.verifikasiAdminAtauGuru,postTabelMapel.tambahMapel);
router.put('/api/v1/putMapel/:id_master_mapel',authVerif.verifikasiAdminAtauGuru,putTabelMapel.editMapel);
router.delete('/api/v1/deleteMapel/:id_master_mapel',authVerif.verifikasiAdminAtauGuru,deleteTabelMapel.deleteMapel);
router.get('/api/v1/getMapelByIdKelas/:id_kelas',authVerif.verifikasiGeneral,getTabelMapel.getMapelByIdKelas);
router.get('/api/v1/getAllMapel',authVerif.verifikasiGeneral,getTabelMapel.getAllMapel);

//tabel tugas
router.post('/api/v1/tambahTugas',authVerif.verifikasiAdminAtauGuru,postTabelTugas.tambahTugas);
router.get('/api/v1/getTugasByIdMapel/:id_mapel/:id_siswa/:status',authVerif.verifikasiGeneral,getTabelTugas.getTugasByIdMapel);
router.get('/api/v1/getTugasByIdTugas/:id_nama_tugas',authVerif.verifikasiGeneral,getTabelTugas.getTugasByIdTugas);
router.get('/api/v1/getTugasByIdKelas/:id_kelas/:id_mapel/:status',authVerif.verifikasiGeneral,getTabelTugas.getTugasByIdKelas);
router.put('/api/v1/putTugasById/',authVerif.verifikasiAdminAtauGuru,putTabelTugas.editTugas);
router.delete('/api/v1/deleteTugasById/:id',authVerif.verifikasiAdminAtauGuru,deleteTabelTugas.deleteTugas);

//tabel nilai
router.post('/api/v1/tambahNilai',authVerif.verifikasiGeneral,post_nilai.tambahNilai);
router.get('/api/v1/getNilai/:id_kelas/:id_mapel/:id_tugas',authVerif.verifikasiGeneral,get_nilai.getNilai);
router.get('/api/v1/getNilaiByidSiswadantugas/:id_siswa/:id_tugas',authVerif.verifikasiGeneral,get_nilai_idsiswa_idtugas.getNilaiByIdSiswaAndTugas);
router.put('/api/v1/putNilaiKeseluruhan',authVerif.verifikasiGeneral,put_nilai_keseluruhan.putNilaiKeseluruhan);

// tabel status tugas
router.post('/api/v1/tambahStatusTugas',authVerif.verifikasiGeneral,postStatusTugas.tambahStatusTugas);
router.get('/api/v1/getStatusTugasByIdTugasDanSiswa/:id_tugas/:id_siswa',authVerif.verifikasiGeneral,getStatusTugas.getStatusTugas);

//tabel soal
router.post('/api/v1/tambahSoal',authVerif.verifikasiGuru,post_soal.tambahSoal);
router.get('/api/v1/getSoalByIdTugas/:id_tugas/:id_kelas',authVerif.verifikasiGeneral, get_soal.getSoalByIdTugas );
router.put('/api/v1/putSoalByIdSoal/:id_soal',authVerif.verifikasiAdminAtauGuru, put_soal.editSoal );
router.delete('/api/v1/deleteSoalByIdSoal/:id_soal',authVerif.verifikasiAdminAtauGuru, delete_soal.deleteSoal );

// tabel kunci jawaban
router.get('/api/v1/getKunciByIdSoal/:id_soal',authVerif.verifikasiAdminAtauGuru, get_kunci.getKunciByIdSoal );

//input jawaban user
router.post('/api/v1/tambahJawabanSiswa',authVerif.verifikasiGeneral,inputjawabanUser.tambahJawabanUser);
router.get('/api/v1/getJawabanSiswa/:id/:id_siswa',authVerif.verifikasiGeneral,getjawabanUser.getJawabanUserByIdSoalDanSiswa);

// get tabel siswa
router.get('/api/v1/getAllSiswa', sequilezeSiswa.siswa.getAll);
router.get('/api/v1/getAllDataSiswa',authVerif.verifikasiAdminTu,  getDataSiswa.getAllDataSiswa);
router.put('/api/v1/putSiswa/:id_pengguna/:id_akun', authVerif.verifikasiAdminTu, putDataSiswa.editSiswa);

// get tabel guru
router.get('/api/v1/getAllGuru', authVerif.verifikasiAdminTu, getDataGuru.getAllDataGuru);

// upload file
router.post('/api/v1/uploadFile',authVerif.verifikasiGeneral,upload.single('file'),postUploadFile.tambahFile);
router.delete('/api/v1/delFile/:id', authVerif.verifikasiGeneral,deleteUploadFile.deleteFile)
router.get('/api/v1/getFile/:id', authVerif.verifikasiGeneral,getFile.getFile)

// list kelas guru
router.post('/api/v1/tambahListKelasGuru', authVerif.verifikasiAdminTu,postListKelasGuru.tambahListKelasGuru);
router.get('/api/v1/getListKelasGuruByIdGuru/:id_guru', authVerif.verifikasiAdminAtauGuru,getListKelasGuru.getListKelasByIdGuru);
router.get('/api/v1/getListKelasGuruByIdGuruForGuru/:id_guru', authVerif.verifikasiAdminAtauGuru,getListKelasGuru.getListKelasByIdGuruForGuru);
router.get('/api/v1/getListMapelKelasGuruByIdKelas/:id_guru/:id_kelas', authVerif.verifikasiAdminAtauGuru,getListKelasGuru.getListMapelKelasByIdKelas);
router.put('/api/v1/editListKelasGuru/:id_list_kelas', authVerif.verifikasiAdminTu,putListKelasGuru.editListKelasGuru);
router.delete('/api/v1/deleteListKelasGuru/:id_list_kelas', authVerif.verifikasiAdminTu,deleteListKelasGuru.deleteListKelasGuru);

// get dokumen
router.use('/dokumen', express.static('./upload/file'),(req,res)=>{
    console.log("cek");
});

module.exports = router