const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.putModelListKelas = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "UPDATE tabel_list_kelas_guru SET id_kelas=?, id_mapel=? WHERE id_list_kelas=?"
        var table = [post.id_kelas,post.id_mapel,post.id_list_kelas];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan"+error);
            } else {
                res.status(200).send("berhasil put data");
            }
        });
    });
}