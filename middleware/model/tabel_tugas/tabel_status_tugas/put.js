const connection = require('../../../../koneksi');
const mysql = require('mysql');

exports.putTabelStatusTugas = async function (res,post,waktu_selesai,status){
    return new Promise(function(resolve,reject){
        var query = "UPDATE status_tugas SET status=?, waktu_selesai=? WHERE id_status_tugas=?"
        var table = [status,waktu_selesai,post.id_status_tugas];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).send("berhasil put data");
            }
        });
    })
};