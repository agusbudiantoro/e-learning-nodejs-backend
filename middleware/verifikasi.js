const jwt = require('jsonwebtoken');
const config = require('../config/secret');
var connection = require('../koneksi');
const response = require('../res');
var mysql = require('mysql');

const tabel_akun = require('./model/tabel_akun');

exports.verifikasiAdminTu = async function(req, rest, next){
    // cek token authorization header
    // console.log(req.headers.authorization);
    var tokenWithBearer = req.headers.authorization;
    if(tokenWithBearer){
        var token = tokenWithBearer.split(' ')[1];
        //verif
        jwt.verify(token, config.secret, function(err, decode){
            if(err){
                return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
            }else{
                //cari token
                var get = {
                    access_token: token
                }
                console.log(`${get.access_token}`);
                //mencari token yg sama
                var query = "SELECT * FROM ?? WHERE ??=?";
                var table = ["tabel_access_token", "access_token", get.access_token];
                query = mysql.format(query,table);
                connection.query(query,async function(error,rows){
                    if(error){
                        return rest.status(405).send({auth:false,message:error});
                    }else{
                        if(rows.length != 0){
                            let resDataAkun = await tabel_akun.getDataAkunByIdAkun(rest,rows[0].id_akun);
                            if(resDataAkun[0].role == 1){
                                req.auth = decode;
                                next();
                                // return rest.status(200).send({auth:true,message:"berhasil"});
                            }else{
                                return rest.status(401).send({auth:false,message:"gagal mengotorisasi role anda!"});
                            }
                        }else{
                            return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                        }
                    }
                });
            }
        });
    }else{
        return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
    }
}

exports.verifikasiGuru = async function(req, rest, next){
        // cek token authorization header
        // console.log(req.headers.authorization);
        var tokenWithBearer = req.headers.authorization;
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
                }else{
                    //cari token
                    var get = {
                        access_token: token
                    }
                    console.log(`${get.access_token}`);
                    //mencari token yg sama
                    var query = "SELECT * FROM ?? WHERE ??=?";
                    var table = ["tabel_access_token", "access_token", get.access_token];
                    query = mysql.format(query,table);
                    connection.query(query,async function(error,rows){
                        if(error){
                            return rest.status(405).send({auth:false,message:error});
                        }else{
                            if(rows.length != 0){
                                let resDataAkun = await tabel_akun.getDataAkunByIdAkun(rest,rows[0].id_akun);
                                if(resDataAkun[0].role == 2){
                                    req.auth = decode;
                                    next();
                                    // return rest.status(200).send({auth:true,message:"berhasil"});
                                }else{
                                    return rest.status(401).send({auth:false,message:"gagal mengotorisasi role anda!"});
                                }
                            }else{
                                return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                            }
                        }
                    });
                }
            });
        }else{
            return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
    }

    exports.verifikasiAdminAtauGuru = async function(req, rest, next){
        // cek token authorization header
        // console.log(req.headers.authorization);
        var tokenWithBearer = req.headers.authorization;
        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];
            //verif
            jwt.verify(token, config.secret, function(err, decode){
                if(err){
                    return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
                }else{
                    //cari token
                    var get = {
                        access_token: token
                    }
                    console.log(`${get.access_token}`);
                    //mencari token yg sama
                    var query = "SELECT * FROM ?? WHERE ??=?";
                    var table = ["tabel_access_token", "access_token", get.access_token];
                    query = mysql.format(query,table);
                    connection.query(query,async function(error,rows){
                        if(error){
                            return rest.status(405).send({auth:false,message:error});
                        }else{
                            if(rows.length != 0){
                                let resDataAkun = await tabel_akun.getDataAkunByIdAkun(rest,rows[0].id_akun);
                                if(resDataAkun[0].role == 2 || resDataAkun[0].role == 1){
                                    req.auth = decode;
                                    next();
                                    // return rest.status(200).send({auth:true,message:"berhasil"});
                                }else{
                                    return rest.status(401).send({auth:false,message:"gagal mengotorisasi role anda!"});
                                }
                            }else{
                                return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                            }
                        }
                    });
                }
            });
        }else{
            return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
        }
    }

exports.verifikasiGeneral = async function(req, rest, next){
    // cek token authorization header
    // console.log(req.headers.authorization);
    var tokenWithBearer = req.headers.authorization;
    if(tokenWithBearer){
        var token = tokenWithBearer.split(' ')[1];
        //verif
        jwt.verify(token, config.secret, function(err, decode){
            if(err){
                return rest.status(401).send({auth:false,message:"token tiddak terdaftar atau expired"});
            }else{
                //cari token
                var get = {
                    access_token: token
                }
                console.log(`${get.access_token}`);
                //mencari token yg sama
                var query = "SELECT * FROM ?? WHERE ??=?";
                var table = ["tabel_access_token", "access_token", get.access_token];
                query = mysql.format(query,table);
                connection.query(query,async function(error,rows){
                    if(error){
                        return rest.status(405).send({auth:false,message:error});
                    }else{
                        if(rows.length != 0){
                            let resDataAkun = await tabel_akun.getDataAkunByIdAkun(rest,rows[0].id_akun);
                            req.auth = decode;
                            next();
                        }else{
                            return rest.status(401).send({auth:false,message:"token tiddak ada di tabel akses token"});
                        }
                    }
                });
            }
        });
    }else{
        return rest.status(401).send({auth:false,message:"token tiddak tersedia"});
    }
}