const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getJawabanByIdSoalDanSiswa = async function (res,isi){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=? AND ??=?"
        var table = ["tabel_jawaban_siswa", "id_soal", isi.id_soal,"id_siswa",isi.id_siswa];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).send("data tidak ditemukan");
            } else {
                res.status(200).json(rows[0]);
            }
        });
    })
};