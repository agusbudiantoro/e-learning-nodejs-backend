
const tabel_materi = require('../../model/tabel_materi/post');

exports.tambahMateri = async function (req, res) {
        console.log(req.file);    
        let body = {
            id_kelas:req.body.id_kelas,
            id_mapel:req.body.id_mapel,
            judul:req.body.judul,
            nama_file:req.file.filename,
            original_name:req.file.originalname
        }
        await tabel_materi.postModelMateri(res,body)
}
