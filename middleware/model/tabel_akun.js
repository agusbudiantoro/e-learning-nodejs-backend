const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.registerAkun = async function(res,post,statusPengguna,pass, idPengguna){
        bcrypt.hash(pass, saltRounds,async function (err, hash) {
            let data = {
                "id_pengguna":idPengguna,
                "identitas_pengguna":(statusPengguna == 3)?post.nis:(statusPengguna == 2)?post.nip:post.nip,
                "password":hash,
                "role":statusPengguna,
                "status":0,
                "type":(statusPengguna == 3)?"NIS":(statusPengguna ==2)?"NIP":"NIP"
            }
            var query = "INSERT INTO ?? SET ?";
            var table = ["tabel_akun"];
            query = mysql.format(query, table);
            connection.query(query, data, function (error, rows) {
                if (error) {
                    res.status(403).send(error);
                } else {
                    res.status(200).send("berhasil menambahkan data pengguna dan akun");
                }
            });
        });
}

exports.updateIdAkun = async function(res,post,statusPengguna,pass,idPengguna){
    bcrypt.hash(pass, saltRounds,async function (err, hash) {
            var query = "UPDATE tabel_akun SET id_pengguna=?, password=? WHERE identitas_pengguna=?";
            var table = [idPengguna, hash,(statusPengguna == 2)?post.nip:(statusPengguna == 1)?post.nip:post.nis];
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update tabel akun",value:error })
                } else {
                    res.status(200).send("menambahkan data guru berhasil tetapi akun sudah ada");
                }
            });
    });
}

exports.updateAkun = async function(res,post){
    bcrypt.hash(post.password, saltRounds,async function (err, hash) {
        console.log(post.password);
            var query = "";
            var table=[];
            if(post.password.length == 0){
                query = await "UPDATE tabel_akun SET identitas_pengguna=? WHERE id_akun=?";
                table = await [post.identitas_pengguna, post.id_akun];
            } else {
                query = await "UPDATE tabel_akun SET identitas_pengguna=?, password=? WHERE id_akun=?";
                table = await [post.identitas_pengguna, hash,post.id_akun];
            }
            
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update tabel akun",value:error })
                } else {
                    res.status(200).send("berhasil update akun");
                }
            });
    });
}

exports.changePassword = async function(res,post){
    bcrypt.hash(post.password, saltRounds,async function (err, hash) {
            var query = "";
            var table=[];
            console.log(post);
            if(post.password.length == 0){
                
            } else {
                query = await "UPDATE tabel_akun SET password=? WHERE id_akun=?";
                table = await [hash,post.id_akun];
            }
            
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update tabel akun",value:error })
                } else {
                    res.status(200).send("berhasil update akun");
                }
            });
    });
}

exports.getDataAkun = async function(res,data,statusPengguna){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "identitas_pengguna", (statusPengguna == 3)?data.nip:(statusPengguna == 2)?data.nip:data.nis];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.getDataAkunByIdAkun = async function(res,id){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "id_akun",id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.delAkun= async function(res,id){
    return new Promise(function(resolve, reject){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["tabel_akun","id_akun",id];

        query = mysql.format(query, table);
        connection.query(query, function (error, rows) {
            if (error) {
                res.status(404).send({ auth: false, message: error })
            } else {
                resolve(rows);
            }
        });
    });
}